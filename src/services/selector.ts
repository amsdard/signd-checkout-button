export const findCheckoutButtonElement = <TElement extends Element & {token: string; sid: string}>(
  sid: string,
  target = document,
): TElement | undefined => target.querySelector<TElement>(`[sid="${sid}"]`) || undefined;
