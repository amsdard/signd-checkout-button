export type SigndEventData = {
  sid: string;
};

export enum SigndEventType {
  AUTH_SUCCESS = 'signd-auth-success',
  AUTH_FAILED = 'signd-auth-failed',
  TRANSACTION_STARTED = 'signd-transaction-started',
  TRANSACTION_DONE = 'signd-transaction-done',
  TRANSACTION_FAILED = 'signd-transaction-failed',
  TRANSACTION_ABORTED = 'signd-transaction-aborted',
}

export type SigndEventHandlers = {
  onAuthSuccess?: (event: CustomEvent<SigndEventData>) => void;
  onAuthFailed?: (event: CustomEvent<SigndEventData>) => void;
  onStarted?: (event: CustomEvent<SigndEventData>) => void;
  onDone?: (event: CustomEvent<SigndEventData>) => void;
  onFailed?: (event: CustomEvent<SigndEventData>) => void;
  onAborted?: (event: CustomEvent<SigndEventData>) => void;
};
