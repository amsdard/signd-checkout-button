export type Subscription = {
  unsubscribe: Function;
};

type Subscriber<T> = (v: T) => unknown;

export class LightweightSubject<T> {
  private subscribers: Subscriber<T>[] = [];

  next(value: T) {
    this.subscribers.forEach((subscriber) => subscriber(value));
  }

  subscribe(fn: Subscriber<T>): Subscription {
    this.subscribers.push(fn);
    return {
      unsubscribe: () => {
        this.subscribers = this.subscribers.filter((s) => s !== fn);
      },
    };
  }

  complete() {
    this.subscribers = [];
  }
}
