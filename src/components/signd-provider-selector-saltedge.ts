import {LitElement, html, property, css, query, PropertyValues} from 'lit-element';
import Awesomplete from 'awesomplete';
import {withLocalization, translate} from '../translatable';
import messages from '../i18n/massages';

class SigndProviderSelectorSaltedge extends withLocalization(LitElement) {
  static get styles() {
    return css`
      :host {
        --base-font-size: 14px;
      }

      input {
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        border: 1px solid #8cb2b8;
        border-radius: 8px;
        color: #3b3838;
        font-size: calc(var(--base-font-size) + 2px);
        height: auto;
        line-height: 20px;
        outline: none;
        padding: 9px 12px;
        -webkit-transition: border-color 0.3s ease-in-out;
        transition: border-color 0.3s ease-in-out;
      }

      .awesomplete [hidden] {
        display: none;
      }

      .awesomplete .visually-hidden {
        position: absolute;
        clip: rect(0, 0, 0, 0);
      }

      .awesomplete {
        display: flex;
        position: relative;
        text-align: left;
        width: 100%;
      }

      .awesomplete > input {
        display: block;
        width: 100%;
      }

      .awesomplete > ul {
        position: absolute;
        left: 0;
        z-index: 1;
        min-width: 100%;
        box-sizing: border-box;
        list-style: none;
        padding: 0;
        margin: 0;
        background: #fff;

        max-height: 300px;
        overflow-y: scroll;
      }

      .awesomplete > ul:empty {
        display: none;
      }

      .awesomplete > ul {
        border-radius: 0.3em;
        margin: 0.2em 0 0;
        background: hsla(0, 0%, 100%, 0.9);
        background: linear-gradient(to bottom right, white, hsla(0, 0%, 100%, 1));
        border: 1px solid rgba(0, 0, 0, 0.3);
        box-shadow: 0.05em 0.2em 0.6em rgba(0, 0, 0, 0.2);
        text-shadow: none;
      }

      @supports (transform: scale(0)) {
        .awesomplete > ul {
          transition: 0.3s cubic-bezier(0.4, 0.2, 0.5, 1.4);
          transform-origin: 1.43em -0.43em;
        }

        .awesomplete > ul[hidden],
        .awesomplete > ul:empty {
          opacity: 0;
          transform: scale(0);
          display: block;
          transition-timing-function: ease;
        }
      }

      /* Pointer */

      .awesomplete > ul:before {
        content: '';
        position: absolute;
        top: -0.43em;
        left: 1em;
        width: 0;
        height: 0;
        padding: 0.4em;
        background: white;
        border: inherit;
        border-right: 0;
        border-bottom: 0;
        -webkit-transform: rotate(45deg);
        transform: rotate(45deg);
      }

      .awesomplete > ul > li {
        position: relative;
        padding: 0.2em 0.5em;
        cursor: pointer;
      }

      .awesomplete > ul > li:hover {
        background-color: #8cb2b8;
        color: white;
      }

      .awesomplete > ul > li[aria-selected='true'] {
        background-color: #8cb2b8;
        color: white;
      }

      .awesomplete mark {
        background: hsl(65, 100%, 50%);
      }

      .awesomplete li:hover mark {
        background: hsl(68, 100%, 41%);
      }

      .awesomplete li[aria-selected='true'] mark {
        background: hsl(86, 100%, 21%);
        color: inherit;
      }
    `;
  }

  @property({type: Array})
  items: {label: string; value: any}[] = [];

  @query('#autocomplete')
  private autocompleteElement?: HTMLInputElement;

  @property({type: Object})
  private autocompleteInstance: Awesomplete | null = null;

  async updated(changedProperties: Map<PropertyKey, unknown>) {
    await this.updateComplete;

    if (changedProperties.has('items')) {
      this.autocompleteInstance!.list = [...this.items];
      this.autocompleteInstance?.evaluate();
    }
  }

  protected firstUpdated(_changedProperties: PropertyValues) {
    this.autocompleteInstance = new Awesomplete(this.autocompleteElement!, {
      filter(): boolean {
        return true;
      },
      replace: function (suggestion: {label: string; value: string}) {
        ((this as Awesomplete).input as HTMLInputElement).value = suggestion.label;
      },
      list: [],
      minChars: 1,
      maxItems: 100,
    });
  }

  render() {
    return html`<input
      id="autocomplete"
      required
      placeholder=${translate(messages.selectProvider)}
      @input="${this.dispatchSearchEvent}"
      @awesomplete-selectcomplete="${this.dispatchChangeEvent}"
    />`;
  }

  private dispatchSearchEvent(e: any) {
    if (e.inputType) {
      const event = new CustomEvent('signd-provider-selector-saltedge-search', {
        bubbles: false,
        composed: true,
        detail: {value: e.target.value},
      });
      this.dispatchEvent(event);
    }
  }

  private dispatchChangeEvent(e: any) {
    const event = new CustomEvent('signd-provider-selector-saltedge-change', {
      bubbles: false,
      composed: true,
      detail: {value: e.text.value},
    });
    this.dispatchEvent(event);
  }
}

const ELEMENT_NAME = 'signd-provider-selector-saltedge';
const signdElement = window.customElements.get(ELEMENT_NAME);
if (!signdElement) {
  window.customElements.define(ELEMENT_NAME, SigndProviderSelectorSaltedge);
}

export {SigndProviderSelectorSaltedge, ELEMENT_NAME};
