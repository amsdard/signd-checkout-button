import * as React from 'react';
import {DemoItem} from '../demo-utils/DemoItem';
import '@signd/checkout-button';
import {
  onCheckoutHandlerBySid,
  SigndCheckoutButton,
  registerListenersBySid,
} from '@signd/checkout-button';
import {generateToken, HandshakeAttributesPayload} from '../../services/handshake';
import {Button, Grid} from '@material-ui/core';
import {ProductRow} from './ProductRow';
import {TotalRow} from './TotalRow';

const generateTokenFactory = (attributes: HandshakeAttributesPayload) => async (
  sid: string,
): Promise<string> => {
  if (!process.env.REACT_APP_SIGND_PROCESS_LOGIN || !process.env.REACT_APP_SIGND_PROCESS_LOGIN) {
    alert(
      'Make sure you have setup .env file with vars REACT_APP_SIGND_PROCESS_LOGIN, REACT_APP_SIGND_PROCESS_LOGIN, REACT_APP_SIGND_API_URL',
    );
  }
  const {token} = await generateToken({
    login: process.env.REACT_APP_SIGND_PROCESS_LOGIN as string,
    token: process.env.REACT_APP_SIGND_PROCESS_TOKEN as string,
    attributes,
  });

  return token;
};

const onCheckout = async (sid: string, price: number): Promise<void> => {
  const attributes: HandshakeAttributesPayload = {
    amount: price * 100,
    currency: 'EUR',
    orderID: sid,
    purpose: `Order ${sid}`,
    recipientHolderName: 'John Doe',
    recipientIBAN: 'AT026616336589448716',
    lang: 'en',
  };

  try {
    await onCheckoutHandlerBySid(sid, generateTokenFactory(attributes));
  } catch (e) {
    console.error(`Generate session token for button ${sid} failed!`);
  }
};

const DEFAULT_CART = {
  product1: {quantity: 0, price: 10.1},
  product2: {quantity: 0, price: 20.0},
};

const demoHTML = `
__code__html
<Grid container direction="column">
  {Object.entries(cart).map(([product, {price}]) => <Grid item key={product}>
    <ProductRow<Products> name={product as Products} price={price} onChange={onCartChange} resetTimestamp={restTimestamp} />
  </Grid>)}
  <Grid item>
    <TotalRow name="Shipping" price={shippingPrice} />
  </Grid>
  <Grid item>
    <TotalRow name="Total" price={total} />
  </Grid>
  <Grid item>
    <signd-checkout-button
      sid={sid}
      api-url={process.env.REACT_APP_SIGND_API_URL as string}
      ref={checkoutButtonRef}
    >
      <p slot="in-progress">Payment in progress...</p>
      <p slot="done">Cool you done</p>
      <p slot="failed">Not good!</p>
      <Button disabled={total - shippingPrice <= 0} variant="outlined" onClick={onCustomButtonCheckoutHandler}>Checkout {total} EUR</Button>
      <Button disabled={total - shippingPrice <= 0} color="secondary" variant="outlined" onClick={reset}>Reset</Button>
    </signd-checkout-button>
  </Grid>
</Grid>
__code__
`;

const demoCode = `
__code__typescript
import * as React from 'react';
import {DemoItem} from '../demo-utils/DemoItem';
import '@signd/checkout-button';
import {onCheckoutHandlerBySid, SigndCheckoutButton, registerListenersBySid} from '@signd/checkout-button';
import {generateToken, HandshakeAttributesPayload} from '../../services/handshake';
import {Button, Grid} from '@material-ui/core';
import {ProductRow} from './ProductRow';
import {TotalRow} from './TotalRow';

const generateTokenFactory = (attributes: HandshakeAttributesPayload) => async (sid: string): Promise<string> => {
  const {token} = await generateToken({
    login: process.env.REACT_APP_SIGND_PROCESS_LOGIN as string,
    token: process.env.REACT_APP_SIGND_PROCESS_TOKEN as string,
    attributes,
  });

  return token;
};

const onCheckout = async (sid: string, price: number): Promise<void> => {
  const attributes: HandshakeAttributesPayload = {
    amount: price * 100,
    currency: 'EUR',
    orderID: sid,
    purpose: \`Order \${sid}\`,
    recipientHolderName: 'John Doe',
    recipientIBAN: 'AT026616336589448716',
    lang: 'en',
  };

  try {
    await onCheckoutHandlerBySid(sid, generateTokenFactory(attributes));
  } catch (e) {
    console.error(\`Generate session token for button \${sid} failed!\`);
  }
};

const DEFAULT_CART = {
  product1: {quantity: 0, price: 10.10},
  product2: {quantity: 0, price: 20.00},
};

type Products = keyof typeof DEFAULT_CART;

enum TrxState {
  DONE = 'done',
  FAILED = 'failed',
};

type Props = {};

const DemoShoppingCart = (props: Props) => {
  const checkoutButtonRef = React.useRef<CheckoutButtionElement>();
  const [trxState, setTrxState] = React.useState<TrxState>();
  const sid = 'CheckoutButton';
  const [cart, addToCart] = React.useState(DEFAULT_CART);
  const [total, setTotal] = React.useState(0);
  const [restTimestamp, setRestTimestamp] = React.useState(0);
  const shippingPrice = 2.20;

  React.useEffect(() => {
    const unsubscribe = registerListenersBySid(sid, {
      onDone: () => setTrxState(TrxState.DONE),
      onFailed: () => setTrxState(TrxState.FAILED),
      onAuthSuccess: () => console.log('Auth success'),
      onAuthFailed: () => console.log('Auth failed'),
      onAborted: () => console.log('Transaction aborted'),
    });

    return () => unsubscribe();
  }, [setTrxState, sid]);

  const onCartChange = React.useCallback((value: number, product: Products) => {
    addToCart(it => {
      if (product in it) {
        return {...it, [product]: {...it[product], quantity: value}};
      }

      return it;
    });
  }, [addToCart]);

  React.useEffect(() => {
    setTotal(Object.values(cart).reduce((acc, it) => acc + (it.price * it.quantity), shippingPrice));
  }, [cart, setTotal]);

  const onCustomButtonCheckoutHandler = React.useCallback(async () => {
    if (checkoutButtonRef.current) {
      await onCheckout(checkoutButtonRef.current.sid, total);
    }
  }, [checkoutButtonRef, total]);

  const reset = React.useCallback(() => {
    addToCart({...DEFAULT_CART});
    setRestTimestamp(Date.now());
  }, [addToCart, setRestTimestamp]);

  return
    <Grid container direction="column">
      {Object.entries(cart).map(([product, {price}]) => <Grid item key={product}>
        <ProductRow<Products> name={product as Products} price={price} onChange={onCartChange} resetTimestamp={restTimestamp} />
      </Grid>)}
      <Grid item>
        <TotalRow name="Shipping" price={shippingPrice} />
      </Grid>
      <Grid item>
        <TotalRow name="Total" price={total} />
      </Grid>
      <Grid item>
        <signd-checkout-button
          sid={sid}
          api-url={SIGND_API_URL as string}
          ref={checkoutButtonRef}
        >
          <p slot="in-progress">Payment in progress...</p>
          <p slot="done">Cool you done</p>
          <p slot="failed">Not good!</p>
          <Button disabled={total - shippingPrice <= 0} variant="outlined" onClick={onCustomButtonCheckoutHandler}>Checkout {total} EUR</Button>
          <Button disabled={total - shippingPrice <= 0} color="secondary" variant="outlined" onClick={reset}>Reset</Button>
        </signd-checkout-button>
      </Grid>
    </Grid>
};

export {DemoShoppingCart};
__code__
`;
type Products = keyof typeof DEFAULT_CART;

enum TrxState {
  DONE = 'done',
  FAILED = 'failed',
}

type Props = {};

const DemoShoppingCart = (props: Props) => {
  const checkoutButtonRef = React.useRef<SigndCheckoutButton>();
  const [, setTrxState] = React.useState<TrxState>();
  const sid = 'CheckoutButton';
  const [cart, addToCart] = React.useState(DEFAULT_CART);
  const [total, setTotal] = React.useState(0);
  const [restTimestamp, setRestTimestamp] = React.useState(0);
  const shippingPrice = 2.2;

  React.useEffect(() => {
    const unsubscribe = registerListenersBySid(sid, {
      onStarted: () => console.log('Transaction started'),
      onDone: () => setTrxState(TrxState.DONE),
      onFailed: () => setTrxState(TrxState.FAILED),
      onAuthSuccess: () => console.log('Auth success'),
      onAuthFailed: () => console.log('Auth failed'),
      onAborted: () => console.log('Transaction aborted'),
    });

    return () => unsubscribe();
  }, [setTrxState, sid]);

  const onCartChange = React.useCallback(
    (value: number, product: Products) => {
      addToCart((it) => {
        if (product in it) {
          return {...it, [product]: {...it[product], quantity: value}};
        }

        return it;
      });
    },
    [addToCart],
  );

  React.useEffect(() => {
    setTotal(Object.values(cart).reduce((acc, it) => acc + it.price * it.quantity, shippingPrice));
  }, [cart, setTotal]);

  const onCustomButtonCheckoutHandler = React.useCallback(async () => {
    if (checkoutButtonRef.current) {
      await onCheckout(checkoutButtonRef.current.sid, total);
    }
  }, [checkoutButtonRef, total]);

  const reset = React.useCallback(() => {
    addToCart({...DEFAULT_CART});
    setRestTimestamp(Date.now());
  }, [addToCart, setRestTimestamp]);

  return (
    <DemoItem title="Shopping Cart" demoCode={demoCode} demoHTML={demoHTML}>
      <h3>Shopping Cart Demo</h3>
      <Grid container direction="column">
        {Object.entries(cart).map(([product, {price}]) => (
          <Grid item key={product}>
            <ProductRow<Products>
              name={product as Products}
              price={price}
              onChange={onCartChange}
              resetTimestamp={restTimestamp}
            />
          </Grid>
        ))}
        <Grid item>
          <TotalRow name="Shipping" price={shippingPrice} />
        </Grid>
        <Grid item>
          <TotalRow name="Total" price={total} />
        </Grid>
        <Grid item>
          <signd-checkout-button
            sid={sid}
            api-url={process.env.REACT_APP_SIGND_API_URL as string}
            ref={checkoutButtonRef}
          >
            <p slot="in-progress">Payment in progress...</p>
            <p slot="done">Cool you done</p>
            <p slot="failed">Not good!</p>
            <Button
              disabled={total - shippingPrice <= 0}
              variant="outlined"
              onClick={onCustomButtonCheckoutHandler}
            >
              Checkout {total} EUR
            </Button>
            <Button
              disabled={total - shippingPrice <= 0}
              color="secondary"
              variant="outlined"
              onClick={reset}
            >
              Reset
            </Button>
          </signd-checkout-button>
        </Grid>
      </Grid>
    </DemoItem>
  );
};

export {DemoShoppingCart};
