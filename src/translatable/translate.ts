import {html, TemplateResult} from 'lit-element';
import {getLocale, getLocaleTranslations} from './store';
import {MessageDescriptor} from './types';

class TSA extends Array implements TemplateStringsArray {
  readonly raw: readonly string[];
  constructor() {
    super();
    this.raw = this;
  }
}

const toTsa = (s: string): TemplateStringsArray => {
  const x = new TSA();
  x.push(s);
  return x;
};

export const translate = ({id, defaultMessage}: MessageDescriptor): string => {
  const translations = getLocaleTranslations(getLocale());
  if (!translations[id]) {
    console.warn(`Can't find translation "${id}" for "${getLocale()}" locale.`);
  }
  return translations[id] || defaultMessage;
};

export const translateHtml = (messageDescriptor: MessageDescriptor): TemplateResult => {
  const message = translate(messageDescriptor);
  return html(toTsa(message));
};
