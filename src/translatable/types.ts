export type MessageDescriptor = {
  id: string;
  defaultMessage: string;
  description?: string;
};
export type TranslationsMap = Record<string, string>;
export type LocalesMap = Record<string, TranslationsMap>;
