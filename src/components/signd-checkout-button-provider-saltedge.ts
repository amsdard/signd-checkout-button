import {LitElement, html, property, css, query} from 'lit-element';
import {ifDefined} from 'lit-html/directives/if-defined';
import './signd-modal';
import './signd-provider-selector-saltedge';
import {withLocalization, translateHtml, translate, setLocale} from '../translatable';
import {messages} from '../i18n/massages';
// tslint:disable-next-line:no-duplicate-imports
import {SigndEventType, SigndEventData} from '../types/event';
import {subscribe} from './subscribe';
import {SlotNameEnum} from './slot-name.enum';
import {saltedgeSepaPaymentTemplate} from './saltedge-sepa-payment-template';
import {splitAt} from './fn-helpers';

enum RenderView {
  New = 'new',
  Configuring = 'configuring',
  InProgress = 'in_progress',
  Failed = 'failed',
  Done = 'done',
}

const arrayDiff = (arr1: string[], arr2: string[]): string[] => {
  return arr1.filter((val) => !arr2.includes(val));
};

const sepaPaymentFieldConfig = Object.fromEntries(
  saltedgeSepaPaymentTemplate.payment_fields.map((val) => [val.name, val]),
);

class SigndCheckoutButtonProviderSaltedge extends withLocalization(LitElement) {
  static get styles() {
    return css`
      @import url('https://fonts.googleapis.com/css2?family=Open+Sans:wght@600&display=swap');

      :host {
        font-family: 'Open Sans', Roboto, Helvetica, Arial, sans-serif;
        --base-font-size: 14px;
        font-size: var(--base-font-size);
      }

      .tooltip {
        position: relative;
        display: inline-block;
        font-size: 14px;
        padding: 5px 11px;
        border-radius: 15px;
        background-color: #8cb2b8;
        font-weight: bold;
        color: white;
      }

      .input-tooltip {
        top: -41px;
        left: calc(100% - 32px);
        margin-bottom: -28px;
      }

      .tooltip .tooltiptext {
        visibility: hidden;
        width: calc(100vw - 80px);
        max-width: 390px;
        background-color: #ebf2f9;
        color: #767171;
        font-weight: normal;
        text-align: left;
        border: 1px solid #8cb2b8;
        border-radius: 6px;
        padding: 10px;

        /* Position the tooltip */
        position: absolute;
        z-index: 1;

        top: 95%;
        right: 0;
      }

      .tooltip:hover .tooltiptext {
        visibility: visible;
      }

      .configuring-modal {
        line-height: normal;
        letter-spacing: normal;
        font-size: 14px;
        display: flex;
        flex-direction: row;
        align-items: center;
        justify-content: center;
        min-height: 100vh;
        width: 100%;
        height: 100%;

        /* SCROLL IOS */
        overflow-y: auto;
        overflow-x: hidden;
        scroll-behavior: smooth;
        -webkit-overflow-scrolling: touch;
        /* Optimize scroll regions by reducing paints */
        will-change: transform;
        -webkit-transform: translate3d(0, 0, 0);
      }

      .payment-card {
        display: flex;
        flex-direction: row;
        align-items: stretch;
      }

      @media (max-width: 768px) {
        .payment-card {
          flex-direction: column;
        }
      }

      .link {
        color: #8cb2b8;
      }

      button {
        margin-top: 4px;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        justify-content: space-between;
        font-family: 'Open Sans', Roboto, Helvetica, Arial, sans-serif;
        font-size: calc(var(--base-font-size) + 2px);
        font-weight: 600;
        background-color: #8cb2b8;
        border: 0;
        border-radius: 8px;
        color: #ffffff;
        cursor: pointer;
        display: inline-block;
        height: auto;
        line-height: 16px;
        overflow: hidden;
        outline: none;
        padding: 11px;
        text-align: center;
        text-decoration: none;
        text-overflow: ellipsis;
        text-transform: uppercase;
        width: 100%;
        max-width: 250px;
      }

      button:hover {
        background-color: #ba2374;
      }

      input {
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        border: 1px solid #8cb2b8;
        border-radius: 8px;
        color: #3b3838;
        font-size: calc(var(--base-font-size) + 2px);
        height: auto;
        line-height: 20px;
        outline: none;
        padding: 9px 12px;
        -webkit-transition: border-color 0.3s ease-in-out;
        transition: border-color 0.3s ease-in-out;
      }

      .payment-summary {
        display: flex;
        flex-direction: column;
        align-items: stretch;
        justify-content: flex-start;
        padding: 10px;
        border-right: 1px solid #e2e9f7;
        background: #afc9cd;

        color: #3b3838;
      }

      .payment-summary .merchant-logo {
        display: flex;
        flex-direction: column;
        align-items: center;
        padding: 40px 20px 20px 20px;
      }

      .payment-summary .merchant-logo img {
        max-width: 80px;
        max-height: 80px;
      }

      .payment-summary .merchant-info {
        display: flex;
        flex-direction: column;
        align-items: flex-start;
        padding: 20px;
      }

      .payment-summary .merchant-info .merchant-name {
        font-weight: bold;
        font-size: calc(var(--base-font-size) + 2px);
      }

      .payment-summary .payment-amount {
        display: flex;
        flex-direction: column;
        align-items: center;

        font-weight: bold;
        font-size: calc(var(--base-font-size) + 4px);
      }

      .payment-summary .watermark {
        display: flex;
        flex: 1;
        flex-direction: row;
        align-items: flex-end;
        justify-content: flex-start;
        font-weight: bold;
        color: #ffffff;
        padding-top: 20px;
      }

      .payment-summary .watermark .watermark-content {
        display: flex;
        flex-direction: row;
        align-items: center;
        justify-content: flex-start;
      }

      .payment-summary .watermark .watermark-content img {
        width: 25px;
      }

      .payment-input {
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        max-width: 450px;
        padding: 10px;
        background: #ebf2f9;
      }

      .payment-input form {
        display: flex;
        flex-direction: column;
        align-items: stretch;
        flex: 1;
      }

      .payment-input a {
        color: #8cb2b8;
      }

      .payment-input form .form-row {
        display: flex;
        flex-direction: column;
        align-items: center;
      }

      .payment-input form .provider-selector {
        display: flex;
        flex-direction: column;
        align-items: flex-end;
        padding: 40px 20px 0 20px;
      }

      .payment-input form .provider-selector .provider-selector-input {
        width: 100%;
      }

      .payment-input form .additional-fields {
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
        align-items: flex-end;
        padding: 10px 20px 0 20px;
      }

      .payment-input form .additional-fields > input {
        width: 100%;
      }

      .payment-input form .buttons {
        display: flex;
        flex-direction: row;
        justify-content: flex-end;
        align-items: center;
        padding-right: 40px;
        padding-top: 60px;
      }

      .payment-input form .buttons a {
        padding-right: 20px;
      }

      .payment-input form .terms-and-conditions {
        display: flex;
        flex-direction: column;
        flex: 1;
        justify-content: flex-end;

        color: #767171;
        font-size: calc(var(--base-font-size) * 0.7);
        padding-top: 20px;
      }

      .payment-input form .terms-and-conditions p {
        text-align: left;
      }

      .payment-input form .additional-fields input {
        margin-bottom: 6px;
        color: #8cb2b8;
      }

      .payment-input form .additional-fields input:valid {
        border: 2px solid green;
      }
    `;
  }

  @property({type: String, attribute: 'api-url'})
  apiURL!: string;

  @property({type: String, attribute: 'sid'})
  sid!: string;

  @property({type: String, attribute: 'token'})
  token: string = '';

  @property({type: String})
  private deviceSessionToken: string = '';

  @query('signd-modal')
  private modalElement?: HTMLElement;

  @property({type: Object})
  private paymentAttributes: {
    configuredPaymentAttributes: string[];
    sessionAttributes: Record<string, unknown>;
  } = {
    configuredPaymentAttributes: [],
    sessionAttributes: {},
  };

  @property({type: Object})
  private providersGroupedByCode: Record<string, any> = {};

  @property({type: Object})
  private providersGroupedByName: Record<string, any> = {};

  @property({type: Array})
  private providerSelectorItems: {label: string; value: string}[] = [];

  @property({type: Array})
  private additionalPaymentFields: string[] = [];

  @property({type: Object})
  private additionalPaymentFieldValues: Record<string, unknown> = {};

  @property({type: String})
  private selectedProviderCode: string | null = null;

  @property({type: String})
  private paymentWindow: WindowProxy | null = null;

  @property({type: String})
  private paymentWindowMessageData: any = null;

  @property({type: String})
  private paymentWindowWatchInterval: NodeJS.Timeout | null = null;

  @property({type: String})
  private paymentStatusWatchInterval: NodeJS.Timeout | null = null;

  @property({type: Boolean})
  private isProviderSelectorValid: boolean = false;

  @property({type: String})
  state: RenderView = RenderView.New;

  @property({type: Object, attribute: 'available-slots'})
  availableSlots: {
    [key in SlotNameEnum]: boolean;
  } = {
    [SlotNameEnum.InProgress]: false,
    [SlotNameEnum.Done]: false,
    [SlotNameEnum.Failed]: false,
  };

  @property({type: String, attribute: 'locale'})
  locale!: string;

  private paymentWindowMessageSubscriptions: Function[] = [];

  private forwardEvent(eventType: SigndEventType) {
    this.dispatchEvent(
      new CustomEvent<SigndEventData>(eventType, {
        detail: {sid: this.sid},
        bubbles: true,
        composed: true,
      }),
    );
  }

  private openPaymentWindow(connectUrl: string, transactionId: number) {
    this.paymentWindowMessageData = null;
    this.paymentWindow = window.open(connectUrl, 'saltedge_connect_window');
    this.subscribeToPaymentWindowStatus();
    this.subscribeToPaymentWindowMessages(transactionId);
  }

  private closePaymentWindow() {
    this.unSubscribeFromPaymentWindowMessages();
    this.unsubscribeFromPaymentWindowStatus();
    this.paymentWindowMessageData = null;
    this.paymentWindow?.close();
    this.paymentWindow = null;
  }

  private subscribeToPaymentWindowMessages(transactionId: number) {
    const subscription = subscribe('message', (event: Event) => {
      const {origin, data} = event as MessageEvent;

      if (origin !== 'https://www.saltedge.com') {
        return;
      }

      const parsedMessageData = JSON.parse(data);

      if (parsedMessageData?.data?.custom_fields?.transactionId !== transactionId) {
        return;
      }

      this.paymentWindowMessageData = parsedMessageData;

      if (this.paymentWindowMessageData?.data?.stage === 'finish') {
        this.closePaymentWindow();
      }
    });

    this.paymentWindowMessageSubscriptions = [
      ...this.paymentWindowMessageSubscriptions,
      subscription,
    ];
  }

  private unSubscribeFromPaymentWindowMessages() {
    this.paymentWindowMessageSubscriptions.forEach((it) => it());
    this.paymentWindowMessageSubscriptions = [];
  }

  subscribeToPaymentWindowStatus() {
    this.paymentWindowWatchInterval = setInterval(() => {
      if (this.paymentWindow?.closed) {
        this.closePaymentWindow();

        if (
          this.state === RenderView.InProgress &&
          !(
            this.paymentWindowMessageData?.data?.stage === 'finish' &&
            this.paymentWindowMessageData?.data?.status === 'accepted'
          )
        ) {
          this.forwardEvent(SigndEventType.TRANSACTION_ABORTED);
        }

        this.resetState();
      }
    }, 100);
  }

  unsubscribeFromPaymentWindowStatus() {
    if (this.paymentWindowWatchInterval) {
      clearInterval(this.paymentWindowWatchInterval);
      this.paymentWindowWatchInterval = null;
    }
  }

  subscribeToPaymentStatus(transactionId: number) {
    this.paymentStatusWatchInterval = setInterval(async () => {
      const res = await (
        await fetch(`${this.apiURL}/vendor/webhook/saltedgeSystemPayment/${transactionId}`, {
          method: 'GET',
          headers: {
            Authorization: `Bearer ${this.deviceSessionToken}`,
            'Content-Type': 'application/json;charset=UTF-8',
          },
        })
      ).json();

      switch (res.state) {
        case 'IN_PROGRESS':
          break;
        case 'SUCCESS':
          if (this.token) {
            this.forwardEvent(SigndEventType.TRANSACTION_DONE);
            this.token = '';
            this.state = RenderView.Done;
            this.closePaymentWindow();
          }
          break;
        case 'ERROR':
          if (this.token) {
            this.forwardEvent(SigndEventType.TRANSACTION_FAILED);
            this.token = '';
            this.state = RenderView.Failed;
            this.closePaymentWindow();
          }
          break;
      }
    }, 1000);
  }

  unsubscribeFromPaymentStatus() {
    if (this.paymentStatusWatchInterval) {
      clearInterval(this.paymentStatusWatchInterval);
      this.paymentStatusWatchInterval = null;
    }
  }

  disconnectedCallback(): void {
    this.unsubscribeFromPaymentStatus();
    this.closePaymentWindow();

    super.disconnectedCallback();
  }

  async updated(changedProperties: Map<PropertyKey, unknown>) {
    await this.updateComplete;

    if (changedProperties.has('token') && this.token) {
      this.unsubscribeFromPaymentStatus();
      this.closePaymentWindow();

      const createDeviceSessionRes = await fetch(`${this.apiURL}/internal/session/device`, {
        method: 'POST',
        headers: {'Content-Type': 'application/json;charset=UTF-8'},
        body: JSON.stringify({
          fingerprint: window.navigator.userAgent,
          implementation: 'web',
          platform: 'desktop',
          sessionToken: this.token,
        }),
      });

      if (createDeviceSessionRes.ok) {
        this.forwardEvent(SigndEventType.AUTH_SUCCESS);
      } else {
        this.forwardEvent(SigndEventType.AUTH_FAILED);
        this.token = '';
        return;
      }

      this.deviceSessionToken = (await createDeviceSessionRes.json()).token;

      await fetch(`${this.apiURL}/internal/verification-process/execute`, {
        method: 'POST',
        headers: this.getHeaders(),
      });

      const paymentSessionAttributes = await (
        await fetch(`${this.apiURL}/vendor/payment/saltedge/payments/attributes`, {
          method: 'GET',
          headers: this.getHeaders(),
        })
      ).json();

      this.paymentAttributes = paymentSessionAttributes;
      this.selectedProviderCode = paymentSessionAttributes.sessionAttributes.providerCode;
      if (paymentSessionAttributes.sessionAttributes.lang) {
        setLocale(paymentSessionAttributes.sessionAttributes.lang);
      }
      this.additionalPaymentFields = this.getAdditionalPaymentFields();

      this.state = RenderView.Configuring;
    }

    if (changedProperties.has('state') && this.token) {
      if (this.state === RenderView.Configuring) {
        if (this.paymentAttributes.sessionAttributes.providerCode) {
          this.setBankSelectorValidity(true);
          await this.fetchAndSaveProviders(
            String(this.paymentAttributes.sessionAttributes.providerCode),
          );
        }
      }
    }
  }

  private async startPayment() {
    const {connectUrl, transactionId} = await (
      await fetch(`${this.apiURL}/vendor/payment/saltedge/payments/connect`, {
        method: 'POST',
        headers: this.getHeaders(),
        body: JSON.stringify({
          locale: 'en',
          providerCode: this.selectedProviderCode,
          paymentAttributes: this.additionalPaymentFieldValues,
        }),
      })
    ).json();

    if (!transactionId) {
      return;
    }

    this.openPaymentWindow(connectUrl, transactionId);
    this.subscribeToPaymentStatus(transactionId);

    this.forwardEvent(SigndEventType.TRANSACTION_STARTED);
    this.state = RenderView.InProgress;
  }

  private async fetchAndSaveProviders(query: string) {
    const providers = await this.fetchProviders(query);
    this.providersGroupedByCode = Object.fromEntries(
      providers.map((val: any) => [val.providerCode, val]),
    );
    this.providersGroupedByName = Object.fromEntries(
      providers.map((val: any) => [val.data.fullName, val]),
    );
    this.providerSelectorItems = Object.values(this.providersGroupedByCode).map((provider) => ({
      value: provider?.providerCode,
      label: provider?.data?.fullName,
    }));
  }

  private async fetchProviders(query: string) {
    const response = await fetch(
      `${this.apiURL}/vendor/payment/saltedge/payments/providers/search?query=${query}`,
      {
        method: 'GET',
        headers: this.getHeaders(),
      },
    );

    if (!response.ok) {
      return [];
    }

    return response.json();
  }

  private setBankSelectorValidity(isValid: boolean) {
    this.isProviderSelectorValid = isValid;
  }

  private getAdditionalPaymentFields() {
    return arrayDiff(
      this.selectedProviderCode
        ? this.providersGroupedByCode[this.selectedProviderCode]?.data?.required_payment_fields
            ?.SEPA || []
        : [],
      this.paymentAttributes.configuredPaymentAttributes || [],
    );
  }

  private async handleProviderFormSubmit(e: any) {
    e.preventDefault();
    e.stopPropagation();

    if (!this.selectedProviderCode) {
      return;
    }

    const form = this.shadowRoot!.querySelector('#provider-form') as HTMLFormElement;

    this.additionalPaymentFieldValues = Object.fromEntries(
      this.additionalPaymentFields.map((field) => [
        field,
        (form.elements.namedItem(field) as HTMLInputElement)?.value,
      ]),
    );

    await this.startPayment();
  }

  private handleModalClose(e: any) {
    e.preventDefault();
    e.stopPropagation();

    this.forwardEvent(SigndEventType.TRANSACTION_ABORTED);
    this.resetState();
  }

  private resetState() {
    this.token = '';
    this.state = RenderView.New;
    this.deviceSessionToken = '';
    this.paymentAttributes = {
      configuredPaymentAttributes: [],
      sessionAttributes: {},
    };
    this.providersGroupedByCode = {};
    this.providersGroupedByName = {};
    this.providerSelectorItems = [];
    this.additionalPaymentFields = [];
    this.additionalPaymentFieldValues = {};
  }

  private async handleProviderSelectorSearch(e: CustomEvent<{value: string}>) {
    this.setBankSelectorValidity(false);
    this.selectedProviderCode = null;
    await this.fetchAndSaveProviders(e.detail.value);
  }

  private async handleProviderSelectorChange(e: CustomEvent<{value: string}>) {
    this.selectedProviderCode = e.detail.value;
    this.additionalPaymentFields = this.getAdditionalPaymentFields();
    this.setBankSelectorValidity(!!this.selectedProviderCode);
  }

  private formattedAmountWithCurrency() {
    const amount = Number(this.paymentAttributes.sessionAttributes.amount) / 100;
    const currency = this.paymentAttributes.sessionAttributes.currency as string;
    return new Intl.NumberFormat(this.locale, {style: 'currency', currency}).format(amount);
  }
  render() {
    console.log('render!', this);
    switch (this.state) {
      case RenderView.New:
        return html`<slot></slot>`;
      case RenderView.Configuring:
        return html`
          <signd-modal is-open="true" @signd-modal-close="${this.handleModalClose}">
            <div slot="body" class="configuring-modal">
              <div class="payment-card">
                <div class="payment-summary">
                  <div class="merchant-logo">
                    ${this.paymentAttributes.sessionAttributes.recipientHolderLogoUrl
                      ? html`<img
                          alt="logo"
                          title="logo"
                          src="${this.paymentAttributes.sessionAttributes.recipientHolderLogoUrl}"
                        />`
                      : ''}
                  </div>
                  <div class="merchant-info">
                    <span class="merchant-name"
                      >${this.paymentAttributes.sessionAttributes.recipientHolderName}</span
                    >
                    <span>${this.paymentAttributes.sessionAttributes.recipientIBAN}</span>
                    <span
                      >${this.paymentAttributes.sessionAttributes.recipientHolderAddressLine1}</span
                    >
                    <span
                      >${this.paymentAttributes.sessionAttributes.recipientHolderAddressLine2}</span
                    >
                  </div>
                  <div class="payment-amount">${this.formattedAmountWithCurrency()}</div>
                  <div class="watermark">
                    <div class="watermark-content">
                      <img
                        alt="logo"
                        title="logo"
                        src="https://signd-assets.s3.eu-north-1.amazonaws.com/SignD-Logo-S-white-transparent.png"
                      />
                      a SignD service
                    </div>
                  </div>
                </div>
                <div class="payment-input">
                  <form id="provider-form" @submit="${this.handleProviderFormSubmit}">
                    <div class="form-row provider-selector">
                      ${this.paymentAttributes.sessionAttributes.providerCode
                        ? html`
                            <input
                              class="provider-selector-input"
                              readonly
                              autocomplete="off"
                              name="providers"
                              .value="${this.selectedProviderCode
                                ? this.providersGroupedByCode[this.selectedProviderCode]?.data
                                    ?.fullName || ''
                                : ''}"
                            />
                          `
                        : html`<signd-provider-selector-saltedge
                            class="provider-selector-input"
                            ?isValid=${this.isProviderSelectorValid}
                            .items=${this.providerSelectorItems}
                            @signd-provider-selector-saltedge-search="${this
                              .handleProviderSelectorSearch}"
                            @signd-provider-selector-saltedge-change="${this
                              .handleProviderSelectorChange}"
                          ></signd-provider-selector-saltedge>`}
                    </div>

                    <div class="form-row additional-fields">
                      ${this.additionalPaymentFields.map((field) => {
                        switch (sepaPaymentFieldConfig[field].nature) {
                          case 'number':
                            return html`<input
                              placeholder="${translate(
                                messages[sepaPaymentFieldConfig[field].name],
                              )}"
                              required
                              pattern=${ifDefined(
                                sepaPaymentFieldConfig[field].extra.validation_regexp || undefined,
                              )}
                              type="number"
                              name="${field}"
                            />`;
                          case 'select':
                            return html`
                              <select name="${field}">
                                <option value="">
                                  Select
                                  ${translate(messages[sepaPaymentFieldConfig[field].name])}...
                                </option>
                                ${sepaPaymentFieldConfig[field].field_options?.map(
                                  (option) =>
                                    html`<option value="${option.option_value}">
                                      ${option.english_name}
                                    </option>`,
                                )}
                              </select>
                            `;
                          case 'text':
                          default:
                            if (field === 'debtor_iban') {
                              return html`<input
                                  @input=${this.handleDebtorIbanInput}
                                  placeholder="${translate(
                                    messages[sepaPaymentFieldConfig[field].name],
                                  )}"
                                  required
                                  pattern=${ifDefined(
                                    sepaPaymentFieldConfig[field].extra.validation_regexp ||
                                      undefined,
                                  )}
                                  type="text"
                                  name="${field}"
                                />
                                <div class="tooltip input-tooltip">
                                  i<span class="tooltiptext"
                                    >${translateHtml(messages.ibanDescription)}
                                  </span>
                                </div>`;
                            }

                            return html`<input
                              placeholder="${sepaPaymentFieldConfig[field].english_name}"
                              required
                              pattern=${ifDefined(
                                sepaPaymentFieldConfig[field].extra.validation_regexp || undefined,
                              )}
                              type="text"
                              name="${field}"
                            />`;
                        }
                      })}
                    </div>

                    <div class="form-row buttons">
                      <a href="javascript:void(0)" @click="${this.handleModalClose}"
                        >${translate(messages.cancelPayment)}</a
                      >
                      <button type="submit">${translate(messages.proceed)}</button>
                    </div>

                    <div class="form-row terms-and-conditions">
                      <p>${translateHtml(messages.legalInfo)}</p>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </signd-modal>
        `;
      case RenderView.InProgress:
        return this.availableSlots['in-progress']
          ? html`<slot name="in-progress"></slot>`
          : html`<slot></slot>`;
      case RenderView.Failed:
        return this.availableSlots['failed']
          ? html`<slot name="failed"></slot>`
          : html`<slot></slot>`;
      case RenderView.Done:
        return this.availableSlots['done'] ? html`<slot name="done"></slot>` : html`<slot></slot>`;
    }
  }

  private getHeaders() {
    return {
      Authorization: `Bearer ${this.deviceSessionToken}`,
      'Content-Type': 'application/json;charset=UTF-8',
    };
  }

  private handleDebtorIbanInput(e: Event) {
    const value = (e.target as HTMLInputElement).value;
    const normalized = value.trim().replace(/[^0-9A-Z]/gi, '');
    const formatted = splitAt(4, normalized.toUpperCase()).join(' ');
    (e.target as HTMLInputElement).value = formatted;
  }
}

const ELEMENT_NAME = 'signd-checkout-button-provider-saltedge';
const signdElement = window.customElements.get(ELEMENT_NAME);
if (!signdElement) {
  window.customElements.define(ELEMENT_NAME, SigndCheckoutButtonProviderSaltedge);
}

export {SigndCheckoutButtonProviderSaltedge, ELEMENT_NAME};
