export const subscribe = (
  eventName: string,
  handler: EventListener,
  target: EventTarget = window,
): Function => {
  target.addEventListener(eventName, handler);

  return () => target.removeEventListener(eventName, handler);
};
