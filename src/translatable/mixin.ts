import {LitElement} from 'lit-element';
import {subscribe} from './store';
import {Subscription} from './subject';

type Constructor<T> = new (...args: any[]) => T;

export const withLocalization = <T extends Constructor<LitElement>>(superClass: T) => {
  class TranslatableLitElement extends superClass {
    private subscription!: Subscription;

    connectedCallback() {
      this.subscription = subscribe(async () => {
        await this.requestUpdate();
      });
      super.connectedCallback();
    }

    disconnectedCallback() {
      this.subscription.unsubscribe();
      super.disconnectedCallback();
    }
  }

  return TranslatableLitElement as T;
};
