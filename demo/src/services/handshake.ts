export type HandshakeAttributesPayload = {
  currency: 'EUR';
  orderID: string;
  purpose: string;
  amount: number;
  recipientIBAN?: string;
  recipientHolderName?: string;
  providerCode?: string;
  lang?: 'en' | 'de';
};

export type HandshakePayload = {
  login: string;
  token: string;
  attributes: HandshakeAttributesPayload;
};

export type HandshakeResponse = {
  token: string;
  uuid: string;
};

const handshakeURL = `${process.env.REACT_APP_SIGND_API_URL}/internal/handshake`;

export const generateToken = async (payload: HandshakePayload): Promise<HandshakeResponse> => {
  try {
    const res = await fetch(handshakeURL, {
      method: 'POST',
      cache: 'no-cache',
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(payload),
    });
    const data = await res.json();
    if (res.status !== 201) {
      console.error(`Request failed`, {res, data});
      throw new Error('Handshake failed');
    }

    return data as HandshakeResponse;
  } catch (e) {
    console.error(`Request failed`, {err: e});
    throw e;
  }
};
