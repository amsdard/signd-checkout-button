import * as React from 'react';
import Markdown from 'react-markdown';
import {DemoCodeHighlight} from './DemoCodeHighlight';
import {makeStyles} from '@material-ui/styles';

const useStyles = makeStyles(() => ({
  rootCodeBlock: {
    maxWidth: '90vw',
    overflowX: 'auto',
    '-webkitOverflowScrolling': 'touch',
    margin: 'auto',
  },
}));

type CodeBlockProps = {
  value: React.ReactChild;
  language?: string;
};

const CodeBlock = ({value, language}: CodeBlockProps) => {
  const classes = useStyles();

  return (
    <div className={classes.rootCodeBlock}>
      <DemoCodeHighlight language={language}>{value}</DemoCodeHighlight>
    </div>
  );
};

type Props = {
  source: string;
};

const DemoMarkdownPreview = ({source}: Props) => {
  const md = React.useMemo(() => {
    return source.split('__code__').join('```');
  }, [source]);

  return (
    <div style={{textAlign: 'left', overflow: 'auto'}}>
      <Markdown source={md} renderers={{code: CodeBlock}} />
    </div>
  );
};

export {DemoMarkdownPreview};
