import {findCheckoutButtonElement} from './selector';

const setTokenToCheckoutButton = (element: EventTarget & {token: string}, token: string) => {
  element.token = token;
};

export const applyToken = (token: string, sid: string): boolean => {
  const element = findCheckoutButtonElement(sid);
  if (element) {
    setTokenToCheckoutButton(element, token);
    return true;
  }

  return false;
};

type GenerateToken = (sid: string) => Promise<string>;

export const onCheckoutHandlerByElement = async (
  element: EventTarget & {token: string; sid: string},
  generateToken: GenerateToken,
) => {
  element.token = await generateToken(element.sid);
};

export const onCheckoutHandlerBySid = async (sid: string, generateToken: GenerateToken) => {
  const element = findCheckoutButtonElement(sid);
  if (!element) {
    throw new Error(`Element signd-checkout-button not found by sid: ${sid}`);
  }
  await onCheckoutHandlerByElement(element, generateToken);
};
