import {findCheckoutButtonElement} from './selector';
import {SigndEventHandlers, SigndEventType} from '../types/event';

type Unsubscribe = () => void;

export const registerListeners = (
  target: EventTarget,
  handlers: SigndEventHandlers,
): Unsubscribe => {
  const handlersMap = {
    [SigndEventType.AUTH_SUCCESS]: handlers.onAuthSuccess,
    [SigndEventType.AUTH_FAILED]: handlers.onAuthFailed,
    [SigndEventType.TRANSACTION_STARTED]: handlers.onStarted,
    [SigndEventType.TRANSACTION_DONE]: handlers.onDone,
    [SigndEventType.TRANSACTION_FAILED]: handlers.onFailed,
    [SigndEventType.TRANSACTION_ABORTED]: handlers.onAborted,
  };

  const subscriptions = Object.entries(handlersMap)
    .filter(([, handler]) => !!handler)
    .map(([eventType, handler]) => {
      target.addEventListener(eventType, handler as EventListener);
      return () => target.removeEventListener(eventType, handler as EventListener);
    });

  return () => {
    subscriptions.forEach((it) => it());
  };
};

export const registerListenersByElement = (
  element: EventTarget,
  handlers: SigndEventHandlers,
): Unsubscribe => {
  return registerListeners(element, handlers);
};

export const registerListenersBySid = (sid: string, handlers: SigndEventHandlers): Unsubscribe => {
  const element = findCheckoutButtonElement(sid);
  if (element) {
    return registerListenersByElement(element, handlers);
  }

  console.error(`CheckoutButtonElement with sid ${sid} was not found`);
  return () => undefined;
};
