# Usage

## Set up

Install package:
```
$ npm install @signd/checkout-button
```

Import `signd-checkout-button`:

```js
import '@signd/checkout-button';
```

In addition, you can import typescript types:
```ts
import {CheckoutButtonElement} from '@signd/checkout-button';
```

## Config

**SIGND_API_URL** possible values:
* https://api.signd.io (LIVE)
* https://api.stg.signd.io (STAGING)
* https://api.integration.signd.io (INTEGRATION)
* https://api.dev.signd.io (DEV)

## API

Selector: `signd-checkout-button`

Required properties:


| Name | Description | Type | Required?  |
| ------------- |----------|------| -----|
| sid | unique and valid HTML selector, used to find an element on the page, included also in `signd-*` event details | string | **Yes** |
| api-url | URL to SignD API | string (one of [**SIGND_API_URL**](#config)) | **Yes** |
| token | Token that you will receive from `SIGND_API_URL/internal/handshake endpoint`  | string | **Yes** (to open payment window) |

## Markup quick example

```html
<signd-checkout-button
  sid="YOUR_UNIQUE_TRANSACTION_ID"
  api-url="https://api.signd.io"
  token="TOKEN_RECEIVED_FROM_SIGND_HANDSHAKE_ENDPOINT"
>
  <button click="onCustomButtonCheckoutHandler">Checkout</button>
</signd-checkout-button>
```

## Step-by-step integration summary

You need to implement following steps to accept payments:
1. Back-end
    1. Create your backend API endpoint. It should return token from `SIGND_API_URL/internal/handshake`
    2. Track payment status either:
        1. via calling endpoint `SIGND_API_URL/results/result-output`
        2. or via webhook (We will send you request on URL provided by you. You need to have properly configured **process** to have this functionality)
2. Front-end
    1. Add <signd-checkout-button> to html markup
    2. Implement button click handler, which needs to call your back-end endpoint and pass received token to your payment button

### Back end

#### Generating session token via SignD API

Send `POST` request to URL `/SIGND_API_URL/internal/handshake`.

Use credentials provided by SignD `login, token` and prepared `attributes`.

Request body:

```ts
{
  login: string;
  token: string;
  attributes: {
    currency: 'EUR'; // required
    orderID: string; // required
    purpose: string; // required
    amount: number; // required. Measured in cents!
    recipientIBAN?: string; // optional, on user's side
    recipientHolderName?: string; // optional, on user's side
  };
};
```

Be careful with `attribute.amount` as it is measured in cents.
So, to create 10 EUR payment you need to set amount to 1000.

Response body:

```ts
{
  token: string; // token that you need to use to make payment
  uuid: string; // session ID of payment. Save it to later track payment processing,
};
```

Response codes:
* 400 - `attributes` are not valid
* 401 - provided `login` or `token` are not valid
* 201 - success

#### Tracking payment status

For both options you will receive **ResultOutput** object.

You will find more details in **SignD SDK & API Reference** (Ask SignD support to obtain such docs).

##### Option 1 - SignD API call

Send GET request to endpoint `SIGND_API_URL/results/result-output`.

You need to set **Authorization** header:

Authorization: Bearer **<token_received_from_handshake>**

##### Option 2 - Webhook

Webhook URL will be used to send payment information on it in case of payment process is configured to use webhooks.

You need to contact SignD support to enable webhook payments for you. 


### Front end

#### Add HTML button to your code

```html
<signd-checkout-button
  sid="YOR_UNIQUE_TRANSACTION_ID"
  api-url="https://api.signd.io"
>
  <button click="onCustomButtonCheckoutHandler">Checkout</button>
</signd-checkout-button>
```

#### Define handler

Handler serves two functions:
* generates a session token
* assigns obtained token to the checkout button

Handler snippet example:

```ts
import {onCheckoutHandlerBySid} from '@signd/checkout-button';

const onCustomButtonCheckoutHandler = () =>
  onCheckoutHandlerBySid(
    'YOUR_UNIQUE_TRANSACTION_ID',
    async (sid: string): Promise<string> => {
      const payload = {
        /**
         * Your payload that will allow to get all needed payment attributes on backend.
         * Probably you will want to put here some order id.
         */
      };

      const {token} = await fetch('YOUR_BACKEND_API_URL_TO_CREATE_SESSION', {
        method: 'POST',
        body: JSON.stringify(payload)
      });

      return token;
    }
  );
```

Handler snippet uses `onCheckoutHandlerBySid` utility function, witch has two parameters:
* sid - finds a target checkout button where generated token has to be assigned
* callback function `(sid: string) => Promise<string>` - sends a request to create a token and returns it

`onCheckoutHandlerBySid` function searches for `<signd-checkout-button />` by `sid` and assigns `token` to it.

`onCheckoutHandlerBySid` function example:

```ts
const signdCheckoutButton: CheckoutButtonElement = findButtonBySid(sid);
signdCheckoutButton.token = token;
```

Once token is provided, payment procedure is being initialized.

#### Event handlers

You have two options to get more info about payment processing:
1. Listen directly on button events via `.addEventListener()`
2. Use helpers provided with library which will allow you to define event handlers

##### Option 1 - listen on button events

Following events are available to listen on:
* signd-auth-success - provided token is authenticated
* signd-auth-failed - provided token is not authenticated
* signd-transaction-started - transaction has been started
* signd-transaction-done - transaction has been done successfully, web_hook event should occur
* signd-transaction-failed - transaction has failed
* signd-transaction-aborted - transaction has been aborted (cancelled by user)

Each event is of type [CustomEvent](https://developer.mozilla.org/en-US/docs/Web/API/CustomEvent) with details of following type:

```ts
{
  sid: string;
};
```

##### Option 2 - use provided helpers

To register event for single button, use `registerListenersBySid` utility function:

```ts
import {registerListenersBySid, registerListeners} from '@signd/checkout-button';

registerListenersBySid('YOR_UNIQUE_TRANSACTION_ID', {
  onStarted: (e: CustomEvent<SigndEventData>) => console.log(`Transaction started for sid ${e.detail.sid}`),
  onDone: (e: CustomEvent<SigndEventData>) => console.log(`Transaction done for sid ${e.detail.sid}`),
  onFailed: (e: CustomEvent<SigndEventData>) => console.log(`Transaction failed for sid ${e.detail.sid}`),
  onAborted: (e: CustomEvent<SigndEventData>) => console.log(`Transaction aborted for sid ${e.detail.sid}`),
  onAuthSuccess: (e: CustomEvent<SigndEventData>) => console.log(`Auth success sid ${e.detail.sid}`),
  onAuthFailed: (e: CustomEvent<SigndEventData>) => console.log(`Auth failed sid ${e.detail.sid}`),
});
```

> Note! In case of more than one checkout button, register events on each button separately by `registerListenersBySid` function or on all listeners at once on `window` global object.
For the latter, obtain handle from event `e.detail.sid` which is unique id previously defined on each `<signd-checkout-button>` element.

Example:

```ts
registerListeners(window, {
  onStarted: (e: CustomEvent<SigndEventData>) => console.log(`Transaction started for sid ${e.detail.sid}`),
  onDone: (e: CustomEvent<SigndEventData>) => console.log(`Transaction done for sid ${e.detail.sid}`),
  onFailed: (e: CustomEvent<SigndEventData>) => console.log(`Transaction failed for sid ${e.detail.sid}`),
  onAborted: (e: CustomEvent<SigndEventData>) => console.log(`Transaction aborted for sid ${e.detail.sid}`),
  onAuthSuccess: (e: CustomEvent<SigndEventData>) => console.log(`Auth success sid ${e.detail.sid}`),
  onAuthFailed: (e: CustomEvent<SigndEventData>) => console.log(`Auth failed sid ${e.detail.sid}`),
});
```

## UI Customization

You can put any child html element inside `signd-checkout-button`.

Also, you can display child element conditionally, basing on payment status.
You will need to set `slot` attribute to achieve this.

Following `slot` attribute values are supported:
* `in-progress` - displayed when payment is in progress
* `done` - displayed when payment is finished
* `failed` - displayed when payment is failed

```html
<signd-checkout-button
  sid="YOR_UNIQUE_TRANSACTION_ID"
  api-url="https://api.signd.io"
>
  <span slot="in-progress">Payment is in progress...</span>
  <span slot="done">Payment complete!</span>
  <span slot="failed">Payment failed :(</span>
  <button click="onCustomButtonCheckoutHandler">Checkout</button>
</signd-checkout-button>
```

If no appropriate slot is defined, markup from default slot is displayed. In above case it will be:
```
<button click="onCustomButtonCheckoutHandler">Checkout</button>
```

## Old browser support

This library is built on the `LitElement` and uses the `Web Components` set of standards, which are supported by all major browsers.
For compatibility with older browsers, load the Web Components polyfills.

```html
<head>
  <!-- Load the polyfills first -->
  <script src="https://unpkg.com/@webcomponents/webcomponentsjs/webcomponents-loader.js"></script>
  <!-- Afterwards, load components -->
  <script type="module" src="SIGND_CHECKOUT_BUTTON.js"></script>
</head>
<bod>
  <signd-checkout-button></signd-checkout-button>
</bod>
```

You can also load polyfills asynchronously:

```html
<script defer src="https://unpkg.com/@webcomponents/webcomponentsjs/webcomponents-loader.js"></script>

<script type="module">
  window.WebComponents = window.WebComponents || {
    waitFor(cb){ addEventListener('WebComponentsReady', cb) }
  }

  WebComponents.waitFor(async () => {
    import('SIGND_CHECKOUT_BUTTON.js');
  });
</script>
<bod>
  <signd-checkout-button></signd-checkout-button>
</bod>
```
