import {SigndCheckoutButton} from './components/signd-checkout-button';
export * from './types/event';
export * from './services/handler';
export * from './services/listener';
export {SigndCheckoutButton};

import {addLocaleTranslation, setLocale} from './translatable';
import de from './i18n/locales/de.json';
import en from './i18n/locales/en.json';

addLocaleTranslation('de', de);
addLocaleTranslation('en', en);
setLocale('en');
