import {LitElement, html, property, css} from 'lit-element';

export type CloseModalEventType = 'signd-modal-close';
export const CloseModalEventType = 'signd-modal-close';

class SigndModal extends LitElement {
  // language=css
  static styles = css`
    :host .modal {
      background-color: #fff;
      z-index: 100;
    }

    .modal {
      position: fixed;
      top: 0;
      right: 0;
      bottom: 0;
      left: 0;
      overflow: hidden;

      display: flex;
      flex-direction: column;
    }

    .modal-body {
    }
  `;

  @property({type: Boolean, attribute: 'is-open'})
  isOpen: boolean = false;

  private originScrollY: string = '';

  protected firstUpdated(): void {
    this.shadowRoot!.addEventListener('scroll', console.log);
  }

  updated(changedProperties: Map<PropertyKey, unknown>) {
    if (changedProperties.has('isOpen')) {
      this.onIsOpenChanged();
    }
  }

  private storeScrollPosition() {
    this.originScrollY = `${window.scrollY}px`;
  }

  private onIsOpenChanged() {
    const body = document.querySelector<HTMLBodyElement>('body') || undefined;
    if (this.isOpen) {
      if (body?.style) {
        this.storeScrollPosition();
        body.style.position = 'fixed';
        body.style.top = `-${this.originScrollY}`;
      }
    } else {
      this.restoreScroll(body);
    }
  }

  private restoreScroll(body?: HTMLBodyElement) {
    if (body?.style) {
      const scrollY = body.style.top;
      body.style.position = '';
      body.style.top = '';
      window.scrollTo(0, parseInt(scrollY || '0', 10) * -1);
    }
  }

  disconnectedCallback() {
    if (this.originScrollY) {
      const body = document.querySelector<HTMLBodyElement>('body') || undefined;
      this.restoreScroll(body);
    }
    super.disconnectedCallback();
  }

  render() {
    // language=html
    return html`
      <div ?is-open="${this.isOpen}" class="modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-body">
          <slot name="body"></slot>
        </div>
      </div>
    `;
  }

  private dispatchCloseModalEvent() {
    const event = new CustomEvent(CloseModalEventType, {bubbles: false, composed: true});
    this.dispatchEvent(event);
  }
}

const ELEMENT_NAME = 'signd-modal';
const signdElement = window.customElements.get(ELEMENT_NAME);
if (!signdElement) {
  window.customElements.define(ELEMENT_NAME, SigndModal);
}

export {SigndModal};
