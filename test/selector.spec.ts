import {findCheckoutButtonElement} from '../src/services/selector';

describe('selector', () => {
  it('querySelector when not found', () => {
    const documentMock = ({
      querySelector: jest.fn(() => null),
    } as unknown) as Document;
    const sid = 'sid';
    const result = findCheckoutButtonElement(sid, documentMock);
    expect(documentMock.querySelector).toBeCalledTimes(1);
    expect(documentMock.querySelector).toHaveBeenCalledWith(`[sid="${sid}"]`);
    expect(result).toBe(undefined);
  });

  it('querySelector', () => {
    const documentMock = ({
      querySelector: jest.fn(() => 1),
    } as unknown) as Document;
    const sid = 'sid';
    const result = findCheckoutButtonElement(sid, documentMock);
    expect(documentMock.querySelector).toBeCalledTimes(1);
    expect(documentMock.querySelector).toHaveBeenCalledWith(`[sid="${sid}"]`);
    expect(result).toBe(1);
  });
});
