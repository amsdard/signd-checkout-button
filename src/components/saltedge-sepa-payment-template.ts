// copied from https://docs.saltedge.com/payment_initiation/v1/#payment_templates-show
export const saltedgeSepaPaymentTemplate = {
  identifier: 'SEPA',
  description: 'SEPA Payment',
  deprecated: false,
  created_at: '2021-05-16T08:58:45Z',
  updated_at: '2021-05-16T08:58:45Z',
  payment_fields: [
    {
      id: '363636363636363630',
      payment_template_id: '29',
      name: 'amount',
      english_name: 'Amount',
      localized_name: 'Amount',
      nature: 'number',
      position: 29,
      extra: {
        validation_regexp: '^[-+]?[0-9]*\\.?[0-9]+$',
      },
      optional: false,
      created_at: '2021-05-16T08:58:45Z',
      updated_at: '2021-05-16T08:58:45Z',
    },
    {
      id: '363636363636363631',
      payment_template_id: '29',
      name: 'debtor_iban',
      english_name: 'Your IBAN',
      localized_name: 'Your IBAN',
      nature: 'text',
      position: 30,
      extra: {
        validation_regexp: '.*',
        // validation_regexp: '^[a-zA-Z]{2}[0-9]{2}[a-zA-Z0-9]{4}[A-Z0-9]{7}([a-zA-Z0-9]?){0,16}$',
      },
      optional: true,
      created_at: '2021-05-16T08:58:45Z',
      updated_at: '2021-05-16T08:58:45Z',
    },
    {
      id: '363636363636363632',
      payment_template_id: '29',
      name: 'creditor_iban',
      english_name: 'Creditor IBAN',
      localized_name: 'Creditor IBAN',
      nature: 'text',
      position: 31,
      extra: {
        validation_regexp: '^[a-zA-Z]{2}[0-9]{2}[a-zA-Z0-9]{4}[A-Z0-9]{7}([a-zA-Z0-9]?){0,16}$',
      },
      optional: false,
      created_at: '2021-05-16T08:58:45Z',
      updated_at: '2021-05-16T08:58:45Z',
    },
    {
      id: '363636363636363633',
      payment_template_id: '29',
      name: 'mode',
      english_name: 'Mode',
      localized_name: 'Mode',
      nature: 'select',
      position: 33,
      extra: {
        validation_regexp: '',
      },
      optional: false,
      created_at: '2021-05-16T08:58:45Z',
      updated_at: '2021-05-16T08:58:45Z',
      field_options: [
        {
          name: 'NORMAL',
          english_name: 'NORMAL',
          localized_name: 'NORMAL',
          option_value: 'NORMAL',
          selected: true,
        },
        {
          name: 'INSTANT',
          english_name: 'INSTANT',
          localized_name: 'INSTANT',
          option_value: 'INSTANT',
          selected: false,
        },
      ],
    },
    {
      id: '363636363636363634',
      payment_template_id: '29',
      name: 'debtor_region',
      english_name: 'Region',
      localized_name: 'Region',
      nature: 'text',
      position: 14,
      extra: {
        validation_regexp: '',
      },
      optional: true,
      created_at: '2021-05-16T08:58:45Z',
      updated_at: '2021-05-16T08:58:45Z',
    },
    {
      id: '363636363636363635',
      payment_template_id: '29',
      name: 'creditor_agent',
      english_name: 'Creditor Agent ID',
      localized_name: 'Creditor Agent ID',
      nature: 'text',
      position: 17,
      extra: {
        validation_regexp: '',
      },
      optional: true,
      created_at: '2021-05-16T08:58:45Z',
      updated_at: '2021-05-16T08:58:45Z',
    },
    {
      id: '363636363636363636',
      payment_template_id: '29',
      name: 'customer_ip_address',
      english_name: 'Customer IP Address',
      localized_name: 'Customer IP Address',
      nature: 'text',
      position: 2,
      extra: {
        validation_regexp: '^\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}$',
      },
      optional: false,
      created_at: '2021-05-16T08:58:45Z',
      updated_at: '2021-05-16T08:58:45Z',
    },
    {
      id: '363636363636363637',
      payment_template_id: '29',
      name: 'debtor_name',
      english_name: 'Name',
      localized_name: 'Name',
      nature: 'text',
      position: 8,
      extra: {
        validation_regexp: '',
      },
      optional: true,
      created_at: '2021-05-16T08:58:45Z',
      updated_at: '2021-05-16T08:58:45Z',
    },
    {
      id: '363636363636363638',
      payment_template_id: '29',
      name: 'creditor_name',
      english_name: 'Creditor Name',
      localized_name: 'Creditor Name',
      nature: 'text',
      position: 16,
      extra: {
        validation_regexp: '',
      },
      optional: false,
      created_at: '2021-05-16T08:58:45Z',
      updated_at: '2021-05-16T08:58:45Z',
    },
    {
      id: '363636363636363639',
      payment_template_id: '29',
      name: 'creditor_address',
      english_name: 'Creditor Address',
      localized_name: 'Creditor Address',
      nature: 'text',
      position: 19,
      extra: {
        validation_regexp: '',
      },
      optional: true,
      created_at: '2021-05-16T08:58:45Z',
      updated_at: '2021-05-16T08:58:45Z',
    },
    {
      id: '363636363636363640',
      payment_template_id: '29',
      name: 'debtor_building_number',
      english_name: 'Debtor Building Number',
      localized_name: 'Debtor Building Number',
      nature: 'text',
      position: 11,
      extra: {
        validation_regexp: '',
      },
      optional: true,
      created_at: '2021-05-16T08:58:45Z',
      updated_at: '2021-05-16T08:58:45Z',
    },
    {
      id: '363636363636363641',
      payment_template_id: '29',
      name: 'customer_longitude',
      english_name: 'Customer Longitude',
      localized_name: 'Customer Longitude',
      nature: 'number',
      position: 7,
      extra: {
        validation_regexp: '^[-+]?[0-9]*\\.?[0-9]+$',
      },
      optional: true,
      created_at: '2021-05-16T08:58:45Z',
      updated_at: '2021-05-16T08:58:45Z',
    },
    {
      id: '363636363636363642',
      payment_template_id: '29',
      name: 'debtor_town',
      english_name: 'Debtor Town',
      localized_name: 'Debtor Town',
      nature: 'text',
      position: 13,
      extra: {
        validation_regexp: '',
      },
      optional: true,
      created_at: '2021-05-16T08:58:45Z',
      updated_at: '2021-05-16T08:58:45Z',
    },
    {
      id: '363636363636363643',
      payment_template_id: '29',
      name: 'creditor_agent_name',
      english_name: 'Creditor Agent Name',
      localized_name: 'Creditor Agent Name',
      nature: 'text',
      position: 18,
      extra: {
        validation_regexp: '',
      },
      optional: true,
      created_at: '2021-05-16T08:58:45Z',
      updated_at: '2021-05-16T08:58:45Z',
    },
    {
      id: '363636363636363644',
      payment_template_id: '29',
      name: 'creditor_street_name',
      english_name: 'Creditor Street Name',
      localized_name: 'Creditor Street Name',
      nature: 'text',
      position: 20,
      extra: {
        validation_regexp: '',
      },
      optional: false,
      created_at: '2021-05-16T08:58:45Z',
      updated_at: '2021-05-16T08:58:45Z',
    },
    {
      id: '363636363636363645',
      payment_template_id: '29',
      name: 'creditor_building_number',
      english_name: 'Creditor Building Number',
      localized_name: 'Creditor Building Number',
      nature: 'text',
      position: 21,
      extra: {
        validation_regexp: '',
      },
      optional: false,
      created_at: '2021-05-16T08:58:45Z',
      updated_at: '2021-05-16T08:58:45Z',
    },
    {
      id: '363636363636363646',
      payment_template_id: '29',
      name: 'creditor_post_code',
      english_name: 'Creditor Post Code',
      localized_name: 'Creditor Post Code',
      nature: 'text',
      position: 22,
      extra: {
        validation_regexp: '',
      },
      optional: false,
      created_at: '2021-05-16T08:58:45Z',
      updated_at: '2021-05-16T08:58:45Z',
    },
    {
      id: '363636363636363647',
      payment_template_id: '29',
      name: 'creditor_town',
      english_name: 'Creditor Town',
      localized_name: 'Creditor Town',
      nature: 'text',
      position: 23,
      extra: {
        validation_regexp: '',
      },
      optional: false,
      created_at: '2021-05-16T08:58:45Z',
      updated_at: '2021-05-16T08:58:45Z',
    },
    {
      id: '363636363636363648',
      payment_template_id: '29',
      name: 'creditor_region',
      english_name: 'Creditor Region',
      localized_name: 'Creditor Region',
      nature: 'text',
      position: 24,
      extra: {
        validation_regexp: '',
      },
      optional: true,
      created_at: '2021-05-16T08:58:45Z',
      updated_at: '2021-05-16T08:58:45Z',
    },
    {
      id: '363636363636363649',
      payment_template_id: '29',
      name: 'creditor_country_code',
      english_name: 'Creditor Country Code',
      localized_name: 'Creditor Country Code',
      nature: 'text',
      position: 25,
      extra: {
        validation_regexp: '^[A-Z]{2}$',
      },
      optional: true,
      created_at: '2021-05-16T08:58:45Z',
      updated_at: '2021-05-16T08:58:45Z',
    },
    {
      id: '363636363636363650',
      payment_template_id: '29',
      name: 'date',
      english_name: 'Payment Date',
      localized_name: 'Payment Date',
      nature: 'text',
      position: 26,
      extra: {
        validation_regexp: '^\\d{4}-\\d{2}-\\d{2}$',
      },
      optional: true,
      created_at: '2021-05-16T08:58:45Z',
      updated_at: '2021-05-16T08:58:45Z',
    },
    {
      id: '363636363636363651',
      payment_template_id: '29',
      name: 'time',
      english_name: 'Payment Time',
      localized_name: 'Payment Time',
      nature: 'text',
      position: 27,
      extra: {
        validation_regexp: '^\\d{2}:\\d{2}(:\\d{2})?$',
      },
      optional: true,
      created_at: '2021-05-16T08:58:45Z',
      updated_at: '2021-05-16T08:58:45Z',
    },
    {
      id: '363636363636363652',
      payment_template_id: '29',
      name: 'customer_ip_port',
      english_name: 'Customer IP Port',
      localized_name: 'Customer IP Port',
      nature: 'number',
      position: 3,
      extra: {
        validation_regexp: '^\\d{1,5}$',
      },
      optional: true,
      created_at: '2021-05-16T08:58:45Z',
      updated_at: '2021-05-16T08:58:45Z',
    },
    {
      id: '363636363636363653',
      payment_template_id: '29',
      name: 'customer_device_os',
      english_name: 'Customer Device OS',
      localized_name: 'Customer Device OS',
      nature: 'text',
      position: 4,
      extra: {
        validation_regexp: '',
      },
      optional: false,
      created_at: '2021-05-16T08:58:45Z',
      updated_at: '2021-05-16T08:58:45Z',
    },
    {
      id: '363636363636363654',
      payment_template_id: '29',
      name: 'customer_latitude',
      english_name: 'Customer Latitude',
      localized_name: 'Customer Latitude',
      nature: 'number',
      position: 6,
      extra: {
        validation_regexp: '^[-+]?[0-9]*\\.?[0-9]+$',
      },
      optional: true,
      created_at: '2021-05-16T08:58:45Z',
      updated_at: '2021-05-16T08:58:45Z',
    },
    {
      id: '363636363636363655',
      payment_template_id: '29',
      name: 'description',
      english_name: 'Description',
      localized_name: 'Description',
      nature: 'text',
      position: 28,
      extra: {
        validation_regexp: '^.{2,1000}$',
      },
      optional: false,
      created_at: '2021-05-16T08:58:45Z',
      updated_at: '2021-05-16T08:58:45Z',
    },
    {
      id: '363636363636363656',
      payment_template_id: '29',
      name: 'end_to_end_id',
      english_name: 'End to End Identification',
      localized_name: 'End to End Identification',
      nature: 'text',
      position: 0,
      extra: {
        validation_regexp: '^.{1,35}$',
      },
      optional: false,
      created_at: '2021-05-16T08:58:45Z',
      updated_at: '2021-05-16T08:58:45Z',
    },
    {
      id: '363636363636363657',
      payment_template_id: '29',
      name: 'reference',
      english_name: 'Payment Reference',
      localized_name: 'Payment Reference',
      nature: 'text',
      position: 0,
      extra: {
        validation_regexp: '^.{1,35}$',
      },
      optional: true,
      created_at: '2021-05-16T08:58:45Z',
      updated_at: '2021-05-16T08:58:45Z',
    },
    {
      id: '363636363636363658',
      payment_template_id: '29',
      name: 'debtor_address',
      english_name: 'Debtor Address',
      localized_name: 'Debtor Address',
      nature: 'text',
      position: 9,
      extra: {
        validation_regexp: '',
      },
      optional: true,
      created_at: '2021-05-16T08:58:45Z',
      updated_at: '2021-05-16T08:58:45Z',
    },
    {
      id: '363636363636363659',
      payment_template_id: '29',
      name: 'debtor_street_name',
      english_name: 'Debtor Street Name',
      localized_name: 'Debtor Street Name',
      nature: 'text',
      position: 10,
      extra: {
        validation_regexp: '',
      },
      optional: true,
      created_at: '2021-05-16T08:58:45Z',
      updated_at: '2021-05-16T08:58:45Z',
    },
    {
      id: '363636363636363660',
      payment_template_id: '29',
      name: 'debtor_post_code',
      english_name: 'Debtor Post Code',
      localized_name: 'Debtor Post Code',
      nature: 'text',
      position: 12,
      extra: {
        validation_regexp: '',
      },
      optional: true,
      created_at: '2021-05-16T08:58:45Z',
      updated_at: '2021-05-16T08:58:45Z',
    },
    {
      id: '363636363636363661',
      payment_template_id: '29',
      name: 'debtor_country_code',
      english_name: 'Debtor Country Code',
      localized_name: 'Debtor Country Code',
      nature: 'text',
      position: 15,
      extra: {
        validation_regexp: '^[A-Z]{2}$',
      },
      optional: true,
      created_at: '2021-05-16T08:58:45Z',
      updated_at: '2021-05-16T08:58:45Z',
    },
    {
      id: '363636363636363662',
      payment_template_id: '29',
      name: 'currency_code',
      english_name: 'Currency',
      localized_name: 'Currency',
      nature: 'select',
      position: 32,
      extra: {
        validation_regexp: '',
      },
      optional: false,
      created_at: '2021-05-16T08:58:45Z',
      updated_at: '2021-05-16T08:58:45Z',
      field_options: [
        {
          name: 'EUR',
          english_name: 'EUR',
          localized_name: 'EUR',
          option_value: 'EUR',
          selected: true,
        },
      ],
    },
    {
      id: '363636363636363663',
      payment_template_id: '29',
      name: 'customer_user_agent',
      english_name: 'Customer User Agent',
      localized_name: 'Customer User Agent',
      nature: 'text',
      position: 5,
      extra: {
        validation_regexp: '',
      },
      optional: true,
      created_at: '2021-05-16T08:58:45Z',
      updated_at: '2021-05-16T08:58:45Z',
    },
    {
      id: '363636363636363664',
      payment_template_id: '29',
      name: 'customer_last_logged_at',
      english_name: 'Customer Last Logged At',
      localized_name: 'Customer Last Logged At',
      nature: 'text',
      position: 1,
      extra: {
        validation_regexp: '',
      },
      optional: true,
      created_at: '2021-05-16T08:58:45Z',
      updated_at: '2021-05-16T08:58:45Z',
    },
  ],
};
