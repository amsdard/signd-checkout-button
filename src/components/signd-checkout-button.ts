import {html, LitElement, property} from 'lit-element';
import './signd-checkout-button-provider-saltedge';
import {SigndCheckoutButtonProviderEnum} from './signd-checkout-button-provider.enum';
import {subscribe} from './subscribe';
import {SlotNameEnum} from './slot-name.enum';
import {setLocale} from '../translatable';

enum RenderView {
  New = 'new',
  InProgress = 'in_progress',
  Failed = 'failed',
  Done = 'done',
}

class SigndCheckoutButton extends LitElement {
  @property({type: String, attribute: 'provider'})
  provider?: SigndCheckoutButtonProviderEnum = SigndCheckoutButtonProviderEnum.SALTEDGE;

  @property({type: String, attribute: 'api-url'})
  apiURL!: string;

  @property({type: String, attribute: 'sid'})
  sid!: string;

  @property({type: String, attribute: 'token'})
  token: string = '';

  @property({type: String, attribute: 'locale'})
  locale: string = 'en';

  private slotChangeEventSubscriptions: Function[] = [];

  @property({type: String})
  availableSlots: {
    [key in SlotNameEnum]: boolean;
  } = {
    [SlotNameEnum.InProgress]: false,
    [SlotNameEnum.Done]: false,
    [SlotNameEnum.Failed]: false,
  };

  updated(changedProperties: Map<string, unknown>) {
    if (changedProperties.has('locale')) {
      setLocale(this.locale);
    }
  }

  firstUpdated(changedProperties: Map<string, unknown>) {
    if (changedProperties.has('locale')) {
      setLocale(this.locale);
    }

    this.shadowRoot!.querySelectorAll('slot').forEach((slot) => {
      this.slotChangeEventSubscriptions = [
        ...this.slotChangeEventSubscriptions,
        subscribe(
          'slotchange',
          (event: Event) => {
            this.availableSlots = {
              ...this.availableSlots,
              [slot.name]: slot.assignedNodes().length > 0,
            };
          },
          slot,
        ),
      ];
    });
  }

  disconnectedCallback(): void {
    this.slotChangeEventSubscriptions.forEach((it) => it());
    this.slotChangeEventSubscriptions = [];

    super.disconnectedCallback();
  }

  render() {
    switch (this.provider) {
      case SigndCheckoutButtonProviderEnum.SALTEDGE:
        return html`
          <signd-checkout-button-provider-saltedge
            api-url=${this.apiURL}
            sid=${this.sid}
            token=${this.token}
            locale=${this.locale}
            available-slots=${JSON.stringify(this.availableSlots)}
          >
            <slot></slot>
            <slot name="in-progress" slot="in-progress"></slot>
            <slot name="failed" slot="failed"></slot>
            <slot name="done" slot="done"></slot>
          </signd-checkout-button-provider-saltedge>
        `;
    }
  }
}

const ELEMENT_NAME = 'signd-checkout-button';
const signdElement = window.customElements.get(ELEMENT_NAME);
if (!signdElement) {
  window.customElements.define(ELEMENT_NAME, SigndCheckoutButton);
}

export {SigndCheckoutButton, ELEMENT_NAME};
