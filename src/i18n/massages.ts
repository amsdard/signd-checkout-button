import {MessageDescriptor} from '../translatable/types';

export const messages: Record<string, MessageDescriptor> = {
  ibanDescription: {
    id: 'ibanDescription',
    defaultMessage: `
Ihre ausgewählte Bank benötigt Ihre IBAN vorausgefüllt um fortfahren zu können.
Bitte geben Sie im Eingabefeld Ihre IBAN ein.
Achtung: Ihre IBAN wird direkt an Ihre Bank übermittelt und zu keinem weiteren Zeitpunkt mehr abgefragt.
Lesen Sie
<a href="http://assets.signd.id/c579b/PIS_know.php" target="_blank">hier</a
>, wieso manche Banken vorab die IBAN-Eingabe verlangen.
<br />
<b>Ihre IBAN befindet sich auch auf Ihrer Zahlungskarte!</b>
`,
  },

  /*
   * Ihre ausgewählte Bank benötigt Ihre IBAN vorausgefüllt um fortfahren zu können. Bitte geben Sie im Eingabefeld Ihre IBAN ein. Achtung: Ihre IBAN wird direkt an Ihre Bank übermittelt und zu keinem weiteren Zeitpunkt mehr abgefragt. Lesen Sie hier, wieso manche Banken vorab die IBAN-Eingabe verlangen.
   *
   * */
  legalInfo: {
    id: 'legalInfo',
    defaultMessage: `
      Lizenzgeber für dieses SignD Service ist das regulierte Zahlungsinstitut
      Dimoco Payment Services GmbH. Die Nutzungsbedingungen für dieses Service
      finden Sie
      <a href="http://assets.signd.id/c579b/PIS_tuc.php" target="_blank">hier</a>.
      Durch Bestätigung der Bankauswahl akzeptieren Sie diese Nutzungsbedingungen
      und willigen in die Verarbeitung Ihrer personenbezogenen Daten Erbringung
      des Zahlungsauslöse- und Kontoinformationsdienste ein.
    `,
  },

  proceed: {
    id: 'proceed',
    defaultMessage: 'Weiter',
  },

  cancelPayment: {
    id: 'cancelPayment',
    defaultMessage: 'Zahlung stornieren',
  },

  selectProvider: {
    id: 'selectProvider',
    defaultMessage: 'Select bank',
  },

  amount: {id: 'amount', defaultMessage: 'Amount'},
  debtor_iban: {id: 'debtor_iban', defaultMessage: 'Your IBAN'},
  creditor_iban: {id: 'creditor_iban', defaultMessage: 'Creditor IBAN'},
  mode: {id: 'mode', defaultMessage: 'Mode'},
  debtor_region: {id: 'debtor_region', defaultMessage: 'Region'},
  creditor_agent: {id: 'creditor_agent', defaultMessage: 'Creditor Agent ID'},
  customer_ip_address: {id: 'customer_ip_address', defaultMessage: 'Customer IP Address'},
  debtor_name: {id: 'debtor_name', defaultMessage: 'Name'},
  creditor_name: {id: 'creditor_name', defaultMessage: 'Creditor Name'},
  creditor_address: {id: 'creditor_address', defaultMessage: 'Creditor Address'},
  debtor_building_number: {
    id: 'debtor_building_number',
    defaultMessage: 'Debtor Building Number',
  },
  customer_longitude: {id: 'customer_longitude', defaultMessage: 'Customer Longitude'},
  debtor_town: {id: 'debtor_town', defaultMessage: 'Debtor Town'},
  creditor_agent_name: {id: 'creditor_agent_name', defaultMessage: 'Creditor Agent Name'},
  creditor_street_name: {
    id: 'creditor_street_name',
    defaultMessage: 'Creditor Street Name',
  },
  creditor_building_number: {
    id: 'creditor_building_number',
    defaultMessage: 'Creditor Building Number',
  },
  creditor_post_code: {id: 'creditor_post_code', defaultMessage: 'Creditor Post Code'},
  creditor_town: {id: 'creditor_town', defaultMessage: 'Creditor Town'},
  creditor_region: {id: 'creditor_region', defaultMessage: 'Creditor Region'},
  creditor_country_code: {
    id: 'creditor_country_code',
    defaultMessage: 'Creditor Country Code',
  },
  date: {id: 'date', defaultMessage: 'Payment Date'},
  time: {id: 'time', defaultMessage: 'Payment Time'},
  customer_ip_port: {id: 'customer_ip_port', defaultMessage: 'Customer IP Port'},
  customer_device_os: {id: 'customer_device_os', defaultMessage: 'Customer Device OS'},
  customer_latitude: {id: 'customer_latitude', defaultMessage: 'Customer Latitude'},
  description: {id: 'description', defaultMessage: 'Description'},
  end_to_end_id: {id: 'end_to_end_id', defaultMessage: 'End to End Identification'},
  reference: {id: 'reference', defaultMessage: 'Payment Reference'},
  debtor_address: {id: 'debtor_address', defaultMessage: 'Debtor Address'},
  debtor_street_name: {id: 'debtor_street_name', defaultMessage: 'Debtor Street Name'},
  debtor_post_code: {id: 'debtor_post_code', defaultMessage: 'Debtor Post Code'},
  debtor_country_code: {id: 'debtor_country_code', defaultMessage: 'Debtor Country Code'},
  currency_code: {id: 'currency_code', defaultMessage: 'Currency'},
  customer_user_agent: {id: 'customer_user_agent', defaultMessage: 'Customer User Agent'},
  customer_last_logged_at: {
    id: 'customer_last_logged_at',
    defaultMessage: 'Customer Last Logged At',
  },
};

export default messages;
