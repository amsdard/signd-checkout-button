type Splittable = string | unknown[];

export const takeFirst = (n: number, v: Splittable): Splittable => {
  if (typeof v === 'string' || Array.isArray(v)) {
    return v.slice(0, n);
  }
  throw new Error('takeFirst(number, value) value must be number or array');
};

export const skipFirst = (n: number, v: Splittable): Splittable => {
  if (typeof v === 'string' || Array.isArray(v)) {
    return v.slice(n, v.length);
  }
  throw new Error('skipFirst(number, value) value must be number or array');
};

export const splitAt = (n: number, v: Splittable, t: unknown[] = []): unknown[] => {
  const [h, l] = [takeFirst(n, v), skipFirst(n, v)];
  if (l.length) {
    if (l.length > n) {
      return splitAt(n, l, [...t, h]);
    }
    return [...t, h, l];
  }
  return [...t, h];
};
