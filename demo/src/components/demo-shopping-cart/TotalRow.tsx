import * as React from 'react';
import {Grid, Divider, Box} from '@material-ui/core';

type Props = {
  name: string;
  price: number;
};

const TotalRow = (props: Props) => {
  const {name, price} = props;

  return (
    <React.Fragment>
      <Grid container>
        <Grid item xs={12}>
          <Divider variant="middle" light />
        </Grid>
      </Grid>
      <Grid container justify={'space-between'}>
        <Grid item>
          <Box pl={2}>
            <p>{name}</p>
          </Box>
        </Grid>
        <Grid item>
          <Box pr={2}>
            <p>{`${price}EUR`}</p>
          </Box>
        </Grid>
      </Grid>
    </React.Fragment>
  );
};

export {TotalRow};
