jest.mock('../src/services/selector');
import {
  registerListeners,
  registerListenersByElement,
  registerListenersBySid,
} from '../src/services/listener';
import {SigndCheckoutButton, SigndEventHandlers} from '../src/checkout-button';
import {findCheckoutButtonElement} from '../src/services/selector';

const mockFindCheckoutButtonElement = (): SigndCheckoutButton => {
  const element = {
    addEventListener: jest.fn(),
    removeEventListener: jest.fn(),
  };
  (findCheckoutButtonElement as jest.Mocked<any>).mockImplementation(() => element);
  return (element as unknown) as SigndCheckoutButton;
};

const mockFindCheckoutButtonElementNotFound = (): null => {
  const element = null;
  (findCheckoutButtonElement as jest.Mocked<any>).mockImplementation(() => element);
  return element;
};

type Target = Window | SigndCheckoutButton;
type Unsubscribe = ReturnType<typeof registerListeners>;
type RegisterFn<T extends Target> = (target: T, handlers: SigndEventHandlers) => Unsubscribe;

describe('listener', () => {
  const testWhenNoHandlers = <T extends Target>(target: T, reg: RegisterFn<T>) => {
    const unsubscribe = reg(target, {});
    expect(typeof unsubscribe).toBe('function');
    expect(target.addEventListener).toBeCalledTimes(0);
    expect(target.removeEventListener).toBeCalledTimes(0);
  };

  const testWhenAllHandlers = <T extends Target>(target: T, reg: RegisterFn<T>) => {
    const unsubscribe = reg(target, {
      onAuthFailed: jest.fn(),
      onAuthSuccess: jest.fn(),
      onFailed: jest.fn(),
      onDone: jest.fn(),
      onAborted: jest.fn(),
    });
    expect(typeof unsubscribe).toBe('function');
    expect(target.addEventListener).toBeCalledTimes(5);
    unsubscribe();
    expect(target.removeEventListener).toBeCalledTimes(5);
  };

  describe('Register listener test', () => {
    let windowLike: Window;
    beforeEach(() => {
      windowLike = ({
        addEventListener: jest.fn(),
        removeEventListener: jest.fn(),
      } as unknown) as Window;
    });
    it('Global registerListeners when no handlers', () => {
      testWhenNoHandlers(windowLike, registerListeners);
    });

    it('Global registerListeners with 5 handlers', () => {
      testWhenAllHandlers(windowLike, registerListeners);
    });
  });

  describe('registerListenersByElement', () => {
    let element: SigndCheckoutButton;
    beforeEach(() => {
      element = ({
        addEventListener: jest.fn(),
        removeEventListener: jest.fn(),
      } as unknown) as SigndCheckoutButton;
    });

    it('registerListenersByElement when no handlers', () => {
      testWhenNoHandlers(element, registerListenersByElement);
    });

    it('registerListenersByElement with 5 handlers', () => {
      testWhenAllHandlers(element, registerListenersByElement);
    });
  });

  describe('registerListenersBySid', () => {
    it('registerListenersBySid when all handlers', () => {
      const element = mockFindCheckoutButtonElement();
      const unsubscribe = registerListenersBySid('sid', {
        onAuthFailed: jest.fn(),
        onAuthSuccess: jest.fn(),
        onStarted: jest.fn(),
        onFailed: jest.fn(),
        onDone: jest.fn(),
        onAborted: jest.fn(),
      });
      expect(typeof unsubscribe).toBe('function');
      expect(element.addEventListener).toBeCalledTimes(6);
      unsubscribe();
      expect(element.removeEventListener).toBeCalledTimes(6);
    });

    it('registerListenersBySid when not found', () => {
      mockFindCheckoutButtonElementNotFound();
      const unsubscribe = registerListenersBySid('sid', {
        onAuthFailed: jest.fn(),
        onAuthSuccess: jest.fn(),
        onStarted: jest.fn(),
        onFailed: jest.fn(),
        onDone: jest.fn(),
        onAborted: jest.fn(),
      });
      expect(typeof unsubscribe).toBe('function');
      expect(unsubscribe()).toBe(undefined);
    });
  });
});
