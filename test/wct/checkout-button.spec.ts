import {SigndCheckoutButton} from '../../src/checkout-button';
import {promisifyFlush} from './utils';

declare const flush: (cb?: Function) => void;
declare const fixture: <T extends HTMLElement = HTMLElement>(id: string, model?: object) => T;

const {assert} = chai;

declare var suite: Mocha.SuiteFunction;
declare var test: Mocha.TestFunction;
declare var setup: Mocha.HookFunction;

suite('signd-checkout-button', () => {
  let element: SigndCheckoutButton;
  let button: HTMLElement;
  const flushCompleted = promisifyFlush(flush);

  suite('signd-checkout-button', () => {
    setup(async () => {
      element = fixture('CheckoutButtonFixture');
      await flushCompleted();
      button = element.querySelector<HTMLElement>('button')!;
    });

    test('checkout button is instantiated properly', () => {
      assert.instanceOf(element, SigndCheckoutButton);
      assert.isEmpty(element.token);
      assert.equal(element.sid, 'test1');
    });

    test('checkout button slot exists', async () => {
      assert.instanceOf(button, HTMLButtonElement);
    });
  });
});
