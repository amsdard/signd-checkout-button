import React, {ReactNode} from 'react';
import {Grid, Box, Button, Divider, Paper} from '@material-ui/core';
import {makeStyles} from '@material-ui/styles';
import {DemoMarkdownPreview} from './DemoMarkdownPreview';

const useStyles = makeStyles(() => ({
  toggleButton: {
    marginBottom: 10,
  },
  box: {
    width: '96vw',
    height: 'auto',
  },
}));

type Props = {
  title: ReactNode;
  children: ReactNode;
  demoCode: string;
  demoHTML: string;
};

const DemoItem = (props: Props) => {
  const {title, children, demoCode, demoHTML} = props;
  const [showCode, setShowCode] = React.useState(false);
  const classes = useStyles();

  const toggleCode = React.useCallback(() => {
    setShowCode((it) => !it);
  }, [setShowCode]);

  return (
    <Box m={3} className={classes.box}>
      <Paper elevation={3}>
        <Grid container direction="column" alignItems="stretch" spacing={2}>
          <Grid item>
            <h3>{title}</h3>
          </Grid>
          {!!demoCode && (
            <Grid item>
              <Button className={classes.toggleButton} variant="outlined" onClick={toggleCode}>
                Toggle Code
              </Button>
              <Divider variant="fullWidth" light />
              <div style={{border: '1px solid gray'}}>
                <DemoMarkdownPreview source={showCode ? demoCode : demoHTML} />
              </div>
            </Grid>
          )}
          <Grid item>{children}</Grid>
        </Grid>
      </Paper>
    </Box>
  );
};

export {DemoItem};
