import * as React from 'react';
import {Grid, Divider, Box, Button} from '@material-ui/core';

type Props<P> = {
  name: P;
  price: number;
  resetTimestamp: number;
  onChange: (value: number, name: P) => void;
};

const ProductRow = <P extends string>(props: Props<P>) => {
  const {name, price, onChange, resetTimestamp = 0} = props;
  const [value, setValue] = React.useState(0);

  const increment = React.useCallback(() => {
    setValue((it) => it + 1);
  }, [setValue]);

  const decrement = React.useCallback(() => {
    setValue((it) => {
      if (it > 0) {
        return it - 1;
      }

      return it;
    });
  }, [setValue]);

  React.useEffect(() => {
    onChange(value, name);
  }, [value, name, onChange]);

  React.useEffect(() => {
    setValue(0);
  }, [resetTimestamp, setValue]);

  return (
    <React.Fragment>
      <Grid container>
        <Grid item xs={12}>
          <Divider variant="middle" light />
        </Grid>
      </Grid>
      <Grid container alignItems="center" justify={'space-between'}>
        <Grid item xs={12} md={8}>
          <Box display="flex" alignItems="center" pl={2} flexGrow={3} textAlign="left">
            <p>{name}</p>
          </Box>
        </Grid>
        <Grid xs={6} md={2} item>
          <Box display="flex" flexDirection="row" alignItems="center" justifyContent="flex-end">
            <Button onClick={increment}>+</Button>
            <p>{value}</p>
            <Button onClick={decrement}>-</Button>
          </Box>
        </Grid>
        <Grid xs={6} md={2} item>
          <Box pr={2} textAlign="right">
            <p>{`${price}EUR`}</p>
          </Box>
        </Grid>
      </Grid>
    </React.Fragment>
  );
};

export {ProductRow};
