import * as React from 'react';
import hljs from 'highlight.js';
import 'highlight.js/styles/atom-one-dark.css';

const LangMap = {
  html: 'xml',
  ts: 'typescript',
  tsx: 'typescript',
  js: 'javascript',
  jsx: 'javascript',
};

const getLanguage = (language = 'javascript'): string => {
  return LangMap[language as keyof typeof LangMap] || language;
};

const style: React.CSSProperties = {
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'stretch',
  textAlign: 'left',
};

type Props = {
  children: React.ReactChild;
  language?: string;
};

const DemoCodeHighlight = ({children, language = 'javascript'}: Props) => {
  const codeRef = React.useRef<HTMLPreElement | null>(null);

  React.useEffect(() => {
    const nodes = codeRef.current?.querySelectorAll('pre code') || [];

    for (let i = 0; i < nodes.length; i++) {
      hljs.highlightBlock(nodes[i]);
    }
  }, [children, codeRef]);

  if (!children || (typeof children === 'string' && !children.trim())) {
    return null;
  }

  return (
    <div style={style}>
      <pre ref={codeRef}>
        <code className={`hljs language-${getLanguage(language)}`}>{children}</code>
      </pre>
    </div>
  );
};

export {DemoCodeHighlight};
