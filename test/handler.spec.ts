jest.mock('../src/services/selector');
import {
  applyToken,
  onCheckoutHandlerByElement,
  onCheckoutHandlerBySid,
} from '../src/services/handler';
import {findCheckoutButtonElement} from '../src/services/selector';
import {SigndCheckoutButton} from '../src/checkout-button';

const SID = 'test';
const TOKEN = 'the-token';
const mockFindCheckoutButtonElement = (): SigndCheckoutButton => {
  const element = {token: undefined, sid: SID};
  (findCheckoutButtonElement as jest.Mocked<any>).mockImplementation(() => element);
  return (element as unknown) as SigndCheckoutButton;
};

const mockFindCheckoutButtonElementNotFound = (): null => {
  const element = null;
  (findCheckoutButtonElement as jest.Mocked<any>).mockImplementation(() => element);
  return element;
};

describe('Handler test', () => {
  it('applyToken', () => {
    const element = mockFindCheckoutButtonElement();
    const result = applyToken('the-token', 'sid');
    expect(element.token).toEqual('the-token');
    expect(result).toBe(true);
  });

  it('applyToken when not find element', () => {
    mockFindCheckoutButtonElementNotFound();
    const result = applyToken('the-token', 'sid');
    expect(findCheckoutButtonElement as jest.Mocked<any>).toBeCalled();
    expect(result).toBe(false);
  });

  it('onCheckoutHandlerByElement', async () => {
    const element = ({token: undefined, sid: SID} as unknown) as SigndCheckoutButton;
    const generateToken = jest.fn(async () => TOKEN);
    await onCheckoutHandlerByElement(element, generateToken);
    expect(element.token).toEqual(TOKEN);
    expect(generateToken).toBeCalledWith(SID);
  });

  it('onCheckoutHandlerBySid', async () => {
    const element = mockFindCheckoutButtonElement();
    const generateToken = jest.fn(async () => 'the-token');
    await onCheckoutHandlerBySid(SID, generateToken);
    expect(element.token).toEqual(TOKEN);
    expect(generateToken).toBeCalledWith(SID);
  });

  it('onCheckoutHandlerBySid NotFound', async () => {
    mockFindCheckoutButtonElementNotFound();
    const generateToken = jest.fn(async () => 'the-token');
    expect(onCheckoutHandlerBySid(SID, generateToken)).rejects.toThrowError(
      `Element signd-checkout-button not found by sid: ${SID}`,
    );
    expect(generateToken).not.toBeCalled();
  });
});
