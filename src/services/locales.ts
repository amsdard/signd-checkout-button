type LocaleTranslations = Record<string, string>;
type MessageDescriptor = {
  id: string;
  defaultMessage: string;
  description?: string;
};
const allLocales: Record<string, LocaleTranslations> = {};
let currentLocale: string = '';

export function setTranslations(locale: string, translations: LocaleTranslations): void {
  allLocales[locale] = translations;
}
export function setLocale(locale: string): void {
  currentLocale = locale;
}

export function formatMessage(
  {id, defaultMessage}: MessageDescriptor,
  {}: Record<string, number | string> = {},
): string {
  return allLocales[currentLocale][id] || defaultMessage;
}
