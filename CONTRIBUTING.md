# Documentation

Documentation is generated in `./docs` directory.

# Committing

Please use conventional commits: https://www.conventionalcommits.org/en/v1.0.0/ 

# Publishing

Pre-release:
```
npm run publish:dev
git push origin --tags
```

Release:
```
npm run publish:prod
git push origin --tags
```

# Contributing

* First, you need to clone repo and build project 
```sh
  git clone git@bitbucket.org:amsdard/signd-checkout-button.git
  cd ./signd-checkout-button
  npm install
  npm run build
```

* Running docs

install any http server
```sh
  npm i -g http-server-spa 
  http-server-spa docs index.html 3333
```

To open docs, visit: `http://localhost:3333`

* Running demo

```sh
  cd ./demo
  npm install
  npm run install:self
```
Create file signd-checkout-button/demo/.env.local and populate
it with data.

After you set all the parameters, run:

```
  npm start
```
