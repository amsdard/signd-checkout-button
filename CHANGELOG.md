# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.3-alpha.7](https://bitbucket.org/amsdard/signd-checkout-button/compare/v0.1.2...v0.1.3-alpha.7) (2021-12-21)

### [0.1.3-alpha.6](https://bitbucket.org/amsdard/signd-checkout-button/compare/v0.1.3-alpha.4...v0.1.3-alpha.6) (2021-08-30)

### [0.1.3-alpha.5](https://bitbucket.org/amsdard/signd-checkout-button/compare/v0.1.3-alpha.4...v0.1.3-alpha.5) (2021-08-30)

### [0.1.3-alpha.4](https://bitbucket.org/amsdard/signd-checkout-button/compare/v0.1.3-alpha.3...v0.1.3-alpha.4) (2021-08-26)

### [0.1.3-alpha.3](https://bitbucket.org/amsdard/signd-checkout-button/compare/v0.1.3-alpha.2...v0.1.3-alpha.3) (2021-08-20)

### [0.1.3-alpha.2](https://bitbucket.org/amsdard/signd-checkout-button/compare/v0.1.3-alpha.1...v0.1.3-alpha.2) (2021-08-20)

### [0.1.3-alpha.1](https://bitbucket.org/amsdard/signd-checkout-button/compare/v0.1.3-alpha.0...v0.1.3-alpha.1) (2021-08-20)

### [0.1.3-alpha.0](https://bitbucket.org/amsdard/signd-checkout-button/compare/v0.1.2...v0.1.3-alpha.0) (2021-08-20)

### [0.1.2](https://bitbucket.org/amsdard/signd-checkout-button/compare/v0.1.2-alpha.10...v0.1.2) (2021-06-11)

### [0.1.2-alpha.10](https://bitbucket.org/amsdard/signd-checkout-button/compare/v0.1.2-alpha.9...v0.1.2-alpha.10) (2021-06-11)


### Bug Fixes

* responsive styling ([f2673ce](https://bitbucket.org/amsdard/signd-checkout-button/commit/f2673ceca0a2b700f9bc8afd202b10ddb042c84c))

### [0.1.2-alpha.9](https://bitbucket.org/amsdard/signd-checkout-button/compare/v0.1.2-alpha.8...v0.1.2-alpha.9) (2021-06-10)


### Bug Fixes

* styling ([eb21a2d](https://bitbucket.org/amsdard/signd-checkout-button/commit/eb21a2d7e6528c5bc633717c8c466d7dc49e5789))

### [0.1.2-alpha.8](https://bitbucket.org/amsdard/signd-checkout-button/compare/v0.1.2-alpha.7...v0.1.2-alpha.8) (2021-06-10)

### [0.1.2-alpha.7](https://bitbucket.org/amsdard/signd-checkout-button/compare/v0.1.2-alpha.6...v0.1.2-alpha.7) (2021-06-10)

### [0.1.2-alpha.6](https://bitbucket.org/amsdard/signd-checkout-button/compare/v0.1.2-alpha.5...v0.1.2-alpha.6) (2021-06-10)


### Bug Fixes

* added signd logo ([2c7cfd9](https://bitbucket.org/amsdard/signd-checkout-button/commit/2c7cfd9a1ef8411c200a008ee4ff14bb7e0ef1f4))
* bank selector full text search support ([9f5ec2d](https://bitbucket.org/amsdard/signd-checkout-button/commit/9f5ec2d1ec8a93b36703336ebda4f7927aa8f169))

### [0.1.2-alpha.5](https://bitbucket.org/amsdard/signd-checkout-button/compare/v0.1.2-alpha.4...v0.1.2-alpha.5) (2021-06-02)


### Bug Fixes

* added intermediate page with terms and conditions ([beccede](https://bitbucket.org/amsdard/signd-checkout-button/commit/beccedeb85c6ceea4de434d85deee58a3d838adb))

### [0.1.2-alpha.4](https://bitbucket.org/amsdard/signd-checkout-button/compare/v0.1.2-alpha.3...v0.1.2-alpha.4) (2021-05-31)


### Bug Fixes

* Adjusted styles, removed labels, added placeholders, changed some texts ([f84a076](https://bitbucket.org/amsdard/signd-checkout-button/commit/f84a076348fabbffe17a926f46eec73b15123507))

### [0.1.2-alpha.3](https://bitbucket.org/amsdard/signd-checkout-button/compare/v0.1.2-alpha.2...v0.1.2-alpha.3) (2021-05-28)


### Bug Fixes

* Remove old SDK payment button from repo and adjust tests to use new button ([1144c75](https://bitbucket.org/amsdard/signd-checkout-button/commit/1144c754b2dd1f5ef88806152b9161bec0eff393))
* Remove old SDK payment button from repo and adjust tests to use new button ([74356b7](https://bitbucket.org/amsdard/signd-checkout-button/commit/74356b7c0f09b063de9fde126ef9361daa269316))

### [0.1.2-alpha.2](https://bitbucket.org/amsdard/signd-checkout-button/compare/v0.1.2-alpha.0...v0.1.2-alpha.2) (2021-05-24)


### Bug Fixes

* removed .prettierrc endofline prop ([d6b25b7](https://bitbucket.org/amsdard/signd-checkout-button/commit/d6b25b7d64900083b1ca1400faca53e635348e51))
* start payment procedure if bank provider already set ([2b7b1ef](https://bitbucket.org/amsdard/signd-checkout-button/commit/2b7b1ef4dd311fa90950b6b5fbb97a175e9207c8))

### [0.1.2-alpha.1](https://bitbucket.org/amsdard/signd-checkout-button/compare/v0.1.2-alpha.0...v0.1.2-alpha.1) (2021-05-24)


### Bug Fixes

* removed .prettierrc endofline prop ([d6b25b7](https://bitbucket.org/amsdard/signd-checkout-button/commit/d6b25b7d64900083b1ca1400faca53e635348e51))
* start payment procedure if bank provider already set ([2b7b1ef](https://bitbucket.org/amsdard/signd-checkout-button/commit/2b7b1ef4dd311fa90950b6b5fbb97a175e9207c8))

### [0.1.2-alpha.0](https://bitbucket.org/amsdard/signd-checkout-button/compare/v0.1.1...v0.1.2-alpha.0) (2021-05-24)


### Features

* added bank selector in case of provider code not set upfront ([bcb548b](https://bitbucket.org/amsdard/signd-checkout-button/commit/bcb548bdddbdf8f5e454ac5280ed6c08391b5b4c))

### [0.1.1](https://bitbucket.org/amsdard/signd-checkout-button/compare/v0.1.0...v0.1.1) (2021-04-27)

## [0.1.0](https://bitbucket.org/amsdard/signd-checkout-button/compare/v0.1.0-alpha.5...v0.1.0) (2021-04-27)


### Bug Fixes

* added demo with button bound to single bank ([aa29c96](https://bitbucket.org/amsdard/signd-checkout-button/commit/aa29c96521c99ddf1ac46fb97341cdde90b4bc5c))

## [0.1.0-alpha.5](https://bitbucket.org/amsdard/signd-checkout-button/compare/v0.1.0-alpha.4...v0.1.0-alpha.5) (2021-04-23)

## [0.1.0-alpha.4](https://bitbucket.org/amsdard/signd-checkout-button/compare/v0.1.0-alpha.3...v0.1.0-alpha.4) (2021-04-23)


### Bug Fixes

* typings ([6a06099](https://bitbucket.org/amsdard/signd-checkout-button/commit/6a0609990529398bd490b8e2974f01639671c275))

## [0.1.0-alpha.3](https://bitbucket.org/amsdard/signd-checkout-button/compare/v0.1.0-alpha.2...v0.1.0-alpha.3) (2021-04-22)

## [0.1.0-alpha.2](https://bitbucket.org/amsdard/signd-checkout-button/compare/v0.1.0-alpha.1...v0.1.0-alpha.2) (2021-04-22)

## [0.1.0-alpha.1](https://bitbucket.org/amsdard/signd-checkout-button/compare/v0.1.0-alpha.0...v0.1.0-alpha.1) (2021-04-22)

## [0.1.0-alpha.0](https://bitbucket.org/amsdard/signd-checkout-button/compare/v0.0.4...v0.1.0-alpha.0) (2021-04-22)


### ⚠ BREAKING CHANGES

* <signd-payment-button> interface changed (use api url instead of sdk url, dropped support for close button title). See GUIDE.md for more details.

### Features

* allow payments without using of SignD Web SDK ([91774c8](https://bitbucket.org/amsdard/signd-checkout-button/commit/91774c8f5e49a660dbe75eff5a85aeb58a8fbfa1))

### [0.0.4](https://bitbucket.org/amsdard/signd-checkout-button/compare/v0.0.3...v0.0.4) (2021-02-05)

### [0.0.3](https://bitbucket.org/amsdard/signd-checkout-button/compare/v0.0.3-alpha.3...v0.0.3) (2021-02-05)

### [0.0.3-alpha.3](https://bitbucket.org/amsdard/signd-checkout-button/compare/v0.0.3-alpha.2...v0.0.3-alpha.3) (2020-11-10)

### [0.0.3-alpha.2](https://bitbucket.org/amsdard/signd-checkout-button/compare/v0.0.3-alpha.1...v0.0.3-alpha.2) (2020-11-10)

### [0.0.3-alpha.1](https://bitbucket.org/amsdard/signd-checkout-button/compare/v0.0.3-alpha.0...v0.0.3-alpha.1) (2020-11-10)

### [0.0.3-alpha.0](https://bitbucket.org/amsdard/signd-checkout-button/compare/v0.0.2...v0.0.3-alpha.0) (2020-06-17)

### [0.0.2](https://bitbucket.org/amsdard/signd-checkout-button/compare/v0.0.2-alpha.2...v0.0.2) (2020-06-17)

### [0.0.2-alpha.2](https://bitbucket.org/amsdard/signd-checkout-button/compare/v0.0.2-alpha.1...v0.0.2-alpha.2) (2020-05-11)


### Bug Fixes

* Unnecessary modal scrollbar ([848dc51](https://bitbucket.org/amsdard/signd-checkout-button/commit/848dc51566833226fe7fe2bc9c1849697e66045e))

### [0.0.2-alpha.1](https://bitbucket.org/amsdard/signd-checkout-button/compare/v0.0.2-alpha.0...v0.0.2-alpha.1) (2020-05-06)


### Bug Fixes

* FullScreen is scrollable on IOS devices ([9f1a470](https://bitbucket.org/amsdard/signd-checkout-button/commit/9f1a47071b06f439d707bf3e3f50aea0cd20cac5))

### [0.0.2-alpha.0](https://bitbucket.org/amsdard/signd-checkout-button/compare/v0.0.1...v0.0.2-alpha.0) (2020-05-02)


### Bug Fixes

* Added polyfills section to README ([6626c1b](https://bitbucket.org/amsdard/signd-checkout-button/commit/6626c1bd82092f03da3b8ce424f1e208b462781b))

### 0.0.1 (2020-05-02)
