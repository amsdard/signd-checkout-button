import {LightweightSubject} from './subject';
import {LocalesMap, TranslationsMap} from './types';

let currentLocale = '';

const translationSubject = new LightweightSubject<string>();

export const setLocale = (locale: string) => {
  translationSubject.next(locale);
};
translationSubject.subscribe((l) => (currentLocale = l));

export const getLocale = () => currentLocale;
export const subscribe = translationSubject.subscribe.bind(translationSubject);

const locales: LocalesMap = {};
export const addLocaleTranslation = (locale: string, translations: TranslationsMap) => {
  locales[locale] = translations;
};
export const getLocaleTranslations = (locale: string): TranslationsMap => {
  if (!locales[locale]) {
    throw new Error(`Local "${locale}" translations are not defined`);
  }
  return locales[locale];
};
