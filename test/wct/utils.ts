import {LitElement} from 'lit-element';
import {SigndEventType, SigndEventData} from '../../src/types/event';

export const promisifyFlush = (flush: Function) => () => new Promise((resolve) => flush(resolve));

type OnEventType = <T extends Event = CustomEvent>(
  eventName: SigndEventType,
) => (element: LitElement, shouldResolve: boolean) => Promise<T>;

const onEvent: OnEventType = (eventName: SigndEventType) => (
  element: LitElement,
  shouldResolve: boolean,
) =>
  new Promise<any>((resolve, reject) => {
    element.addEventListener(eventName, (event: Event) =>
      shouldResolve ? resolve(event) : reject(event),
    );
  });

export const onAuthSuccess = onEvent<CustomEvent<SigndEventData>>(SigndEventType.AUTH_SUCCESS);
export const onAuthFailed = onEvent<CustomEvent<SigndEventData>>(SigndEventType.AUTH_FAILED);
export const onDone = onEvent<CustomEvent<SigndEventData>>(SigndEventType.TRANSACTION_DONE);
export const onFailed = onEvent<CustomEvent<SigndEventData>>(SigndEventType.TRANSACTION_FAILED);
export const onAborted = onEvent<CustomEvent<SigndEventData>>(SigndEventType.TRANSACTION_ABORTED);

export const deffer = <T>(data: T, timeout: number): Promise<T> =>
  new Promise((r) => setTimeout(() => r(data), timeout));
