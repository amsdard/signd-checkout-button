import React from 'react';
import './App.css';
import '@signd/checkout-button';
import {SigndCheckoutButton} from '@signd/checkout-button'; // eslint-disable-line @typescript-eslint/no-unused-vars
import {DemoSingleButton} from './components/demo-single-button/DemoSingleButton';
import {DemoShoppingCart} from './components/demo-shopping-cart/DemoShoppingCart';
import {Button} from '@material-ui/core';

declare global {
  namespace JSX {
    interface IntrinsicElements {
      'signd-checkout-button': {
        provider?: string;
        sid: string;
        'api-url': string;
        token?: string;
        children?: any;
        ref?: React.MutableRefObject<SigndCheckoutButton | undefined>;
      };
    }
  }
}

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <DemoSingleButton>
          {(clickHandler: any) => {
            return (
              <Button variant="outlined" onClick={clickHandler}>
                Checkout
              </Button>
            );
          }}
        </DemoSingleButton>
        <DemoSingleButton title="Bound to specific bank" providerCode="fake_client_xf">
          {(clickHandler: any) => {
            return (
              <Button variant="outlined" onClick={clickHandler}>
                <img alt="Fake bank" src={'/fake-bank.png'} />
              </Button>
            );
          }}
        </DemoSingleButton>
        <DemoSingleButton withCustomization title="Single button with customization">
          {(clickHandler: any) => {
            return (
              <Button variant="outlined" onClick={clickHandler}>
                Checkout
              </Button>
            );
          }}
        </DemoSingleButton>
        <DemoShoppingCart />
      </header>
    </div>
  );
}

export default App;
