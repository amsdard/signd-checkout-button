import * as React from 'react';
import {DemoItem} from '../demo-utils/DemoItem';
import '@signd/checkout-button';
import {
  onCheckoutHandlerBySid,
  SigndCheckoutButton,
  registerListenersBySid,
} from '@signd/checkout-button';
import {generateToken, HandshakeAttributesPayload} from '../../services/handshake';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

const demoCode = `
__code__javascript
import * as React from 'react';
import {DemoItem} from '../demo-utils/DemoItem';
import '@signd/checkout-button';
import {onCheckoutHandlerBySid, SigndCheckoutButton, registerListenersBySid} from '@signd/checkout-button';
import {generateToken, HandshakeAttributesPayload} from '../../services/handshake';
import {Button} from '@material-ui/core';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

const generateTokenFactory = (attributes: HandshakeAttributesPayload) => async (sid: string): Promise<string> => {
  const {token} = await generateToken({
    login: process.env.REACT_APP_SIGND_PROCESS_LOGIN as string,
    token: process.env.REACT_APP_SIGND_PROCESS_TOKEN as string,
    attributes,
  });

  return token;
};

const onCheckout = async (sid: string): Promise<void> => {
  const attributes: HandshakeAttributesPayload = {
    amount: 1000,
    currency: 'EUR',
    orderID: sid,
    purpose: \`Order \${sid}\`,
    recipientHolderName: 'John Doe',
    recipientIBAN: 'AT026616336589448716',
    lang: 'en',
  };

  try {
    await onCheckoutHandlerBySid(sid, generateTokenFactory(attributes));
  } catch (e) {
    console.error(\`Generate session token for button \${sid} failed!\`);
  }
};

enum TrxState {
  DONE = 'done',
  FAILED = 'failed',
};

type Props = {
  withCustomization?: boolean;
};

const DemoSingleButton = ({withCustomization}: Props) => {
  const singleButtonRef = React.useRef<CheckoutButtionElement>();
  const [trxState, setTrxState] = React.useState<TrxState>();
  const sid = \`TrxSingleButton\${withCustomization ? '2' : '1'}\`;

  React.useEffect(() => {
    const unsubscribe = registerListenersBySid(sid, {
      onDone: () => setTrxState(TrxState.DONE),
      onFailed: () => setTrxState(TrxState.FAILED),
      onAuthSuccess: () => console.log('Auth success'),
      onAuthFailed: () => console.log('Auth failed'),
      onAborted: () => console.log('Transaction aborted'),
    });

    return () => unsubscribe();
  }, [setTrxState]);


  const onCustomButtonCheckoutHandler = React.useCallback(async () => {
    if (singleButtonRef.current) {
      await onCheckout(singleButtonRef.current.sid);
    }
  }, [singleButtonRef]);

  return
    <signd-checkout-button
      sid={sid}
      api-url={process.env.REACT_APP_SIGND_API_URL as string}
      ref={singleButtonRef}
    >
      {!!withCustomization && <p slot="in-progress">Payment in progress...</p>}
      {!!withCustomization && <p slot="done">Cool you done</p>}
      {!!withCustomization && <p slot="failed">Not good!</p>}
      <Button variant="outlined" onClick={onCustomButtonCheckoutHandler}>Checkout</Button>
    </signd-checkout-button>;
};

export {DemoSingleButton};
__code__
`;

const demoHTML = `
__code__html
<signd-checkout-button
  sid={sid}
  api-url={SIGND_API_URL as string}
>
  <Button variant="outlined" onClick={onCustomButtonCheckoutHandler}>Checkout</Button>
</signd-checkout-button>
__code__
`;

const demoHTML2 = `
__code__html
<signd-checkout-button
  sid={sid}
  api-url={SIGND_API_URL as string}
>
  <p slot="in-progress">Payment in progress...</p>
  <p slot="done">Cool you done</p>
  <p slot="failed">Not good!</p>
  <Button variant="outlined" onClick={onCustomButtonCheckoutHandler}>Checkout</Button>
</signd-checkout-button>
__code__
`;

const generateTokenFactory = (attributes: HandshakeAttributesPayload) => async (
  sid: string,
): Promise<string> => {
  const {token} = await generateToken({
    login: process.env.REACT_APP_SIGND_PROCESS_LOGIN as string,
    token: process.env.REACT_APP_SIGND_PROCESS_TOKEN as string,
    attributes,
  });

  return token;
};

const onCheckout = async (sid: string, providerCode?: string): Promise<void> => {
  const attributes: HandshakeAttributesPayload = {
    amount: 1000,
    currency: 'EUR',
    orderID: sid,
    purpose: `Order ${sid}`,
    recipientHolderName: 'John Doe',
    recipientIBAN: 'AT026616336589448716',
    providerCode,
    lang: 'en',
  };

  try {
    await onCheckoutHandlerBySid(sid, generateTokenFactory(attributes));
  } catch (e) {
    console.error(`Generate session token for button ${sid} failed!`);
  }
};

enum TrxState {
  DONE = 'done',
  FAILED = 'failed',
  ABORTED = 'aborted',
}

type Props = {
  withCustomization?: boolean;
  title?: React.ReactNode;
  providerCode?: string;
  children?: (clickHandler: any) => React.ReactNode;
};

const DemoSingleButton = ({withCustomization, title, providerCode, children}: Props) => {
  const singleButtonRef = React.useRef<SigndCheckoutButton>();
  const [trxState, setTrxState] = React.useState<TrxState>();
  const [open, setOpen] = React.useState(false);
  const sid = `TrxSingleButton${withCustomization ? '2' : '1'}`;

  React.useEffect(() => {
    const unsubscribe = registerListenersBySid(sid, {
      onStarted: () => console.log('Transaction started'),
      onDone: () => setTrxState(TrxState.DONE),
      onFailed: () => setTrxState(TrxState.FAILED),
      onAuthSuccess: () => console.log('Auth success'),
      onAuthFailed: () => console.log('Auth failed'),
      onAborted: () => setTrxState(TrxState.ABORTED),
    });

    return () => unsubscribe();
  }, [setTrxState, sid]);

  React.useEffect(() => {
    if (trxState) {
      setOpen(true);
    }
  }, [setOpen, trxState]);

  const onCustomButtonCheckoutHandler = React.useCallback(async () => {
    if (singleButtonRef.current) {
      await onCheckout(singleButtonRef.current.sid, providerCode);
    }
  }, [singleButtonRef, providerCode]);

  const handleClose = React.useCallback(
    (event?: React.SyntheticEvent, reason?: string) => {
      if (reason === 'clickaway') {
        return;
      }

      setOpen(false);
    },
    [setOpen],
  );

  return (
    <DemoItem
      title={title || 'Single Button'}
      demoCode={demoCode}
      demoHTML={withCustomization ? demoHTML2 : demoHTML}
    >
      <Snackbar open={open} autoHideDuration={4000} onClose={handleClose}>
        <MuiAlert
          variant="filled"
          onClose={handleClose}
          severity={trxState === TrxState.FAILED ? 'error' : 'success'}
        >
          {trxState === TrxState.DONE ? 'Transaction done' : ''}
          {trxState === TrxState.FAILED ? 'Transaction failed' : ''}
          {trxState === TrxState.ABORTED ? 'Transaction aborted' : ''}
        </MuiAlert>
      </Snackbar>

      <signd-checkout-button
        sid={sid}
        api-url={process.env.REACT_APP_SIGND_API_URL as string}
        ref={singleButtonRef}
      >
        {!!withCustomization && <p slot="in-progress">Payment in progress...</p>}
        {!!withCustomization && <p slot="done">Cool you done</p>}
        {!!withCustomization && <p slot="failed">Not good!</p>}
        {children?.(onCustomButtonCheckoutHandler)}
      </signd-checkout-button>
    </DemoItem>
  );
};

export {DemoSingleButton};
