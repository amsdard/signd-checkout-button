export enum SlotNameEnum {
  InProgress = 'in-progress',
  Done = 'done',
  Failed = 'failed',
}
