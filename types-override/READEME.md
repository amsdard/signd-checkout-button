We need to have jest and web-component-tester with mocha.
Currently jest is not working fine with lit-element and we have to install @typings/jest and @typings/mocha
Unfortunately both typings are in conflict. To tackle this problem we have to override on of this typings.
By default we are using jest so in `tsconfig.json` we have

```
    "typeRoots": [
      "types-override/mocha",
      "node_modules/@types"
    ],
```

which will provide empty file with typings for mocha `mocha/index.d.ts`

In case when we need mocha but jest is not in use we are using custom tsconfig `tsconfig.wct.test.json`
where we have
```
    "typeRoots": [
      "types-override/jest",
      "node_modules/@types"
    ]
```
this time we are providing empty file with typing for jest `jest/index.d.ts`
